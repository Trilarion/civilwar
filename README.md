# Greenius' Civil War

Originally developed at https://sourceforge.net/projects/civilwar/ by [greenius](https://sourceforge.net/u/greenius/) and published under GPL 2.0.

Conversion of the CVS repository at a.cvs.sourceforge.net::cvsroot/civilwar/ (https://sourceforge.net/projects/civilwar/) to Git with cvs2git (http://cvs2svn.tigris.org/cvs2git.html).

Added preceding history of Civilwar which was stored in the Wargamer CVS (https://sourceforge.net/projects/wargamer/).