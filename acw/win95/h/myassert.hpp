/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef MYASSERT_H
#define MYASSERT_H

#ifndef __cplusplus
#error myassert.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	My Assert Macro
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 14:08:48  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 * Revision 1.7  1995/11/14 11:25:57  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1995/11/13 11:11:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1995/10/29 16:02:49  Steven_Green
 * Added GeneralError
 *
 * Revision 1.4  1995/10/20 11:05:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1995/10/18 10:59:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1995/10/17 12:03:08  Steven_Green
 * debugMessage added
 *
 * Revision 1.1  1995/10/16 11:33:16  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "mytypes.h"

#ifdef DEBUG

extern void _myAssert(int value, char* expr, char* file, int line);
extern void _myDebugMessage(const char* fmt, ...);
extern void _setDebugInfo(char* file, int line);

extern void debugLog(const char* fmt, ...);

#define ASSERT(expr)	\
	if(expr)		\
	{				\
	}				\
	else			\
		_myAssert(expr, #expr, __FILE__,__LINE__)

#define debugMessage _setDebugInfo(__FILE__, __LINE__); _myDebugMessage

#else

#define ASSERT(expr) ((void)0)

inline debugMessage(const char* fmt, ...) { }
// inline debugLog(const char* fmt, ...) { }

#endif

void alert(const char* title, const char* fmt, ...);

class GeneralError {
	static Boolean doingError;
	static Boolean firstError;
	char *describe;
public:
	GeneralError();
	GeneralError(const char* s, ...);

	const char* get();
protected:
	void setMessage(const char* s);
};

class QuitProgram {
};

#endif /* MYASSERT_H */

