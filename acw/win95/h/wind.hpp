/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef WIND_H
#define WIND_H

#ifndef __cplusplus
#error wind.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Base Class for handling Windows call back functions
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 14:08:48  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 * Revision 1.8  1996/02/22 10:29:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1996/02/15 10:51:21  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1996/01/26 11:04:02  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1995/11/22 10:45:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1995/11/13 11:11:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1995/10/20 11:05:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1995/10/18 10:59:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1995/10/16 11:33:16  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <windows.h>
#include <windowsx.h>
#include "myassert.hpp"

/*
 * This is used by:
 *		register a class with:
 *			wc.cbWndExtra = sizeof(WindowBase*)
 *			wc.lpfnWndProc = baseWindProc;
 *
 *		Set up a window setting the lpParam as a pointer to a WindowBase
 *
 *	e.g.
 *
 * WindowBase* newWindow = new WindowBase;
 *
 *	HWND hWnd = CreateWindow(
 *		szMapClass,
 *			"MyClass",
 *			WS_CHILD | WS_CAPTION | WS_BORDER | WS_SYSMENU | WS_VISIBLE | WS_MINIMIZEBOX,
 *			10,
 *			10,
 *			128,
 *			128,
 *			parent,
 *			(HMENU) WID_MyID,
 *			instance,
 *			newWindow
 *		);
 *
 */

class WindowBase {
protected:
	HWND hWnd;			// Window Handle
public:
	WindowBase();
	virtual ~WindowBase() { }

	HWND getHWND() const { return hWnd; }

protected:
	virtual LRESULT defProc(HWND h, UINT m, WPARAM w, LPARAM l) { return DefWindowProc(h, m, w, l); }
	static LRESULT CALLBACK baseWindProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

	/*
	 * Overload CreateWindow function
	 * to make sure that this pointer is passed to it.
	 * Calling it with the derived base's "this" does not work.
	 */

	HWND createWindow(
		DWORD  dwExStyle,
	   LPCTSTR  lpClassName,
		LPCTSTR  lpWindowName,
		DWORD  dwStyle,
		int  x,
		int  y,
		int  nWidth,
		int  nHeight,
		HWND  hWndParent,
		HMENU  hMenu,
		HINSTANCE  hInstance)
	{
		return ::CreateWindowEx(
			dwExStyle,
	   	lpClassName,
			lpWindowName,
		  	dwStyle,
			x,
			y,
			nWidth,
			nHeight,
		 	hWndParent,
		  	hMenu,
			hInstance,
			this);
	}

#ifdef DEBUG_WINDOW_CREATION
	HWND CreateWindowEx(
		DWORD  dwExStyle,
	   LPCTSTR  lpClassName,
		LPCTSTR  lpWindowName,
		DWORD  dwStyle,
		int  x,
		int  y,
		int  nWidth,
		int  nHeight,
		HWND  hWndParent,
		HMENU  hMenu,
		HINSTANCE  hInstance,
		LPVOID param)
	{
		throw GeneralError("WindowBase::CreateWindowEx() called...\nUse createWindow instead\n");
	}
#endif

private:
	virtual LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
};

/*
 * Add this to a window class if it is a child
 */

class ChildWindow {
protected:
	int onMouseActivate(HWND hwnd, HWND hwndTopLevel, UINT codeHitTest, UINT msg);
};

void alterWindowSize(HWND hWnd, LONG lWidth, LONG lHeight, CREATESTRUCT* lpCreateStruct);

/*
 * Some common macros or inline functions
 */

inline HINSTANCE wndInstance(HWND hWnd)
{
	return (HINSTANCE) GetWindowLong(hWnd, GWL_HINSTANCE);
}

/*
 * A Basic Class to use with Popup Windows
 */

class PopupWindow : public WindowBase {
	static const char className[];
	static ATOM classAtom;
public:
	PopupWindow(HINSTANCE hinst) { registerClass(hinst); }

	static ATOM registerClass(HINSTANCE hinst);
	static LPCSTR getClassName() { return className; }
};

extern const char BUTTONCLASS[];
extern const char STATICCLASS[];
extern const char MDICLIENTCLASS[];

#endif /* WIND_H */

