/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef WMISC_H
#define WMISC_H

#ifndef __cplusplus
#error wmisc.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Miscellaneous support classes
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 14:08:48  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 * Revision 1.2  1996/02/16 17:34:14  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1995/10/18 11:06:49  Steven_Green
 * Initial revision
 *
 * Revision 1.1  1995/10/18 10:59:53  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

class ResString {
	LPSTR text;
public:
	ResString(UINT resId);
	~ResString();

	operator LPSTR() { return text; }
};

void wTextOut(HDC hdc, int x, int y, LPCTSTR text);
void PASCAL wTextPrintf(HDC hdc, int x, int y, LPCTSTR text, ...);

#endif /* WMISC_H */


