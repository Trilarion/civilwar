/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Main Window for Civil War Autorun
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 14:08:48  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "globals.hpp"

#include "mainwind.hpp"
#include "autorun.hpp"
#include "resdef.h"
#include "wmisc.hpp"
// #include "bmp.hpp"
#include "readbmp.hpp"
#include "dib.hpp"

/*
 * This is not in Windowsx.h
 */

#define HANDLE_WM_EXITMENULOOP(hwnd, wParam, lParam, fn) \
    (LRESULT)(DWORD)(int)(fn)((hwnd), (BOOL)(wParam))

/*
 * Constants
 */

#define CAPTION_BUFSIZE 50

char szMainCaption[CAPTION_BUFSIZE];
static const char MainWindow::className[] = "CW_Autorun";
static ATOM MainWindow::classAtom = 0;

/*
 * Button IDs
 */

enum {
	BID_INSTALL,
	BID_PLAY,
	BID_QUIT,
	BID_README,
	BID_UPDATES,

	BID_HowMany
};

/*
 * Register Main Class
 */


ATOM MainWindow::registerClass()
{
	if(!classAtom)
	{
		WNDCLASS wc;

		wc.style = CS_HREDRAW | CS_VREDRAW;
		wc.lpfnWndProc = baseWindProc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = sizeof(MainWindow*);
		wc.hInstance = gApp.getInstance();
		wc.hIcon = LoadIcon(gApp.getInstance(), MAKEINTRESOURCE(ICON_AUTORUN));
		wc.hCursor = LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground = (HBRUSH) (COLOR_BACKGROUND + 1);
		// wc.hbrBackground = NULL;	// (HBRUSH) (COLOR_BACKGROUND + 1);
		wc.lpszMenuName = NULL;		// MAKEINTRESOURCE(MENU_START);
		wc.lpszClassName = getClassName();	// szMainClass;	// szAppName;
		classAtom = RegisterClass(&wc);
	}

	ASSERT(classAtom != 0);

	return classAtom;
}

MainWindow::MainWindow(int nCmdShow)
{
	bmp = NULL;
	// dib = 0;
	btOn = NULL;
	btOff = NULL;
	btDisabled = NULL;
	installProcess = NULL;

	instButton = NULL;
#if !defined(DISABLE_PLAY)
	playButton = NULL;
#endif
	quitButton = NULL;
	readmeButton = NULL;
	updateButton = NULL;

	gameDir = 0;

	LoadString(gApp.getInstance(), IDS_Caption, szMainCaption, sizeof(szMainCaption));

	HWND hWnd = createWindow(
		WS_EX_CONTEXTHELP,
		getClassName(),			// szMainClass,
		szMainCaption,
		WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX,
		CW_USEDEFAULT,      		/* init. x pos */
		CW_USEDEFAULT,      		/* init. y pos */
		CW_USEDEFAULT,      		/* init. x size */
		CW_USEDEFAULT,      		/* init. y size */
		NULL,               		/* parent window */
		NULL,               		/* menu handle */
		gApp.getInstance() 		/* program handle */
	);

	ASSERT(hWnd != NULL);
	if(hWnd == NULL)
		return;

	SetWindowText(hWnd, szMainCaption);

	// UpdateWindow(hWnd);
	ShowWindow(hWnd, SW_SHOW);
}


MainWindow::~MainWindow()
{
}

BOOL MainWindow::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
#ifdef DEBUG
	debugLog("+MainWindow::onCreate(%p)\n", hWnd);
#endif

#if 0
	ResString fileName(IDS_STARTUP_BMP);

	if(!readBMP(fileName, RBMP_ForcePalette, &dib))
		return FALSE;

 	int bmpWidth = dib->getWidth();
	int bmpHeight = dib->getHeight();
#else

	HPALETTE hpal;
	bmp = LoadResourceBitmap(gApp.getInstance(), MAKEINTRESOURCE(BM_STARTUP), &hpal);
	ASSERT(bmp != NULL);
	ASSERT(hpal != NULL);
	if(bmp == NULL)
		return FALSE;
	gApp.setPalette(hpal);

	BITMAP bm;
	GetObject(bmp, sizeof(BITMAP), (LPSTR) &bm);

 	int bmpWidth = bm.bmWidth;
	int bmpHeight = bm.bmHeight;

#endif

#if 0
	btOn = LoadBitmap(gApp.getInstance(), MAKEINTRESOURCE(BM_BUTTON_ON));
	btOff = LoadBitmap(gApp.getInstance(), MAKEINTRESOURCE(BM_BUTTON_OFF));
	btDisabled = LoadBitmap(gApp.getInstance(), MAKEINTRESOURCE(BM_BUTTON_DISABLED));
#else
	btOn 		  = LoadResourceBitmap(gApp.getInstance(), MAKEINTRESOURCE(BM_BUTTON_ON), &hpal);
	btOff 	  = LoadResourceBitmap(gApp.getInstance(), MAKEINTRESOURCE(BM_BUTTON_OFF), &hpal);
	btDisabled = LoadResourceBitmap(gApp.getInstance(), MAKEINTRESOURCE(BM_BUTTON_DISABLED), &hpal);
#endif

#if 0			// Bollocks to all this... just read it in myself
	/*
	 * Load in Bitmap
	 */

	bmp = LoadBitmap(gApp.getInstance(), MAKEINTRESOURCE(BM_STARTUP));
	ASSERT(bmp != NULL);
	if(bmp == NULL)
		return FALSE;

	/*
	 * Find out how big it is
	 */

#if 0
	BITMAPCOREHEADER bmh;
	bmh.bcSize = sizeof(BITMAPCOREHEADER);
	bmh.bcWidth = 0;			// Clear values
	bmh.bcHeight = 0;
	bmh.bcPlanes = 0;
	bmh.bcBitCount = 0;

	HDC hdc = CreateCompatibleDC(NULL);

	if(GetDIBits(hdc, bmp, 0, 0, NULL, (LPBITMAPINFO) &bmh, DIB_RGB_COLORS) == 0)
		return FALSE;

	int bmpWidth = bmh.bcWidth;
	int bmpHeight = bmh.bcHeight;
#elif 0
	SIZE bmpSize;
	if(!GetBitmapDimensionEx(bmp, &bmpSize))
		return FALSE;

	int bmpWidth = bmpSize.cx;
	int bmpHeight = bmpSize.cy;
#else
	int bmpWidth = 494;
	int bmpHeight = 325;
#endif
#endif

	alterWindowSize(hWnd, bmpWidth, bmpHeight, lpCreateStruct);

	/*
	 * Make buttons
	 */

	instButton = addButton(hWnd, BID_INSTALL, 	10, 100, IDS_INSTALL);
#if !defined(DISABLE_PLAY)
	playButton = addButton(hWnd, BID_PLAY, 		10, 130, IDS_PLAY);
#endif
	quitButton = addButton(hWnd, BID_QUIT, 		10, 170, IDS_QUIT);
	readmeButton = addButton(hWnd, BID_README, 	144, 100, IDS_README);
	updateButton = addButton(hWnd, BID_UPDATES,	144, 130, IDS_UPDATES);

#if !defined(DISABLE_PLAY)
	getPlayDirectory();
#endif

#ifdef DEBUG
	debugLog("-MainWindow::onCreate()... End\n");
#endif

	return TRUE;
}

void MainWindow::onDestroy(HWND hWnd)
{
#ifdef DEBUG
	debugLog("+MainWindow::onDestroy()\n");
#endif

#if 1
	if(bmp)
	{
		DeleteBitmap(bmp);
		bmp = NULL;
	}
#else
	if(dib)
	{
		delete dib;
		dib = 0;
	}
#endif

	if(btOn)
	{
		DeleteBitmap(btOn);
		btOn = NULL;
	}
	if(btOff)
	{
		DeleteBitmap(btOff);
		btOff = NULL;
	}
	if(btDisabled)
	{
		DeleteBitmap(btDisabled);
		btDisabled = NULL;
	}

	// Consider terminating installProcess

	PostQuitMessage(0);

#ifdef DEBUG
	debugLog("-MainWindow::onDestroy()... End\n");
#endif
}

BOOL MainWindow::onQueryNewPalette(HWND hWnd)
{
	if(gApp.getPalette())
	{
		HDC hDC = GetDC(hWnd);
		HPALETTE hOldPal = SelectPalette(hDC, gApp.getPalette(), FALSE);
		int i = RealizePalette(hDC);       // Realize drawing palette.

		if(i)                         // Did the realization change?
			InvalidateRect(hWnd, NULL, TRUE);    // Yes, so force a 
																// repaint.
		ReleaseDC(hWnd, hDC);
		return i;
	}
	else
		return 0;
}


void MainWindow::onClose(HWND hWnd)
{
	DestroyWindow(hWnd);
}

void MainWindow::onPaint(HWND hWnd)
{
#ifdef DEBUG
	debugLog("+MainWindow::onPaint()\n");
#endif

	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);

#if 0
	ASSERT(dib != 0);

	HPALETTE oldPal = SelectPalette(hdc, gApp.getPalette(), FALSE);
	RealizePalette(hdc);

	SelectObject(gApp.getDibDC(), dib->getHandle());
	BitBlt(hdc, 0,0, dib->getWidth(), dib->getHeight(), gApp.getDibDC(), 0,0, SRCCOPY);

#else
	ASSERT(bmp != NULL);

	RECT cRect;
	GetClientRect(hWnd, &cRect);

	HDC srcDC = gApp.getDibDC();
	HBITMAP oldbm = (HBITMAP) SelectObject(srcDC, bmp);
	BitBlt(hdc, 0,0,cRect.right-cRect.left,cRect.bottom-cRect.top, srcDC, 0, 0, SRCCOPY);
	SelectObject(srcDC, oldbm);
#endif

#if 0
	RECT cRect;
	GetClientRect(hWnd, &cRect);

	HPALETTE oldPal = SelectPalette(hdc, gApp.getPalette(), FALSE);
	RealizePalette(hdc);

	// Copy the DIB onto screen

	SelectObject(gApp.getDibDC(), dib->getHandle());
	BitBlt(hdc, 0,0, dib->getWidth(), dib->getHeight(), gApp.getDibDC(), 0,0, SRCCOPY);

	/*
	 * Put on rectangle of where we are in parent
	 */

	MapWindow* mw = getMapWindow();

	HPEN pen;

	if(dragging)
		pen = CreatePen(PS_SOLID, 1, RGB(255,0,255));
	else
	{
		zoomArea.left  	 = (mw->whereTop.getX() * (cRect.right  - cRect.left)) / mw->fullSize.getX();
		zoomArea.top   	 = (mw->whereTop.getY() * (cRect.bottom - cRect.top))  / mw->fullSize.getY();
		zoomArea.right 	 = (mw->whereSize.getX() * dib->getWidth())  / mw->fullSize.getX();
		zoomArea.bottom	 = (mw->whereSize.getY() * dib->getHeight()) / mw->fullSize.getY();
		pen = CreatePen(PS_SOLID, 1, RGB(0,0,0));
	}
	
	int x1 = zoomArea.left;
	int y1 = zoomArea.top;
	int x2 = x1 + zoomArea.right - 1;
	int y2 = y1 + zoomArea.bottom - 1;

	HPEN oldPen = (HPEN) SelectObject(hdc, pen);

	POINT oldP;
	MoveToEx(hdc, x1, y1, &oldP);
	LineTo(hdc, x2, y1);
	LineTo(hdc, x2, y2);
	LineTo(hdc, x1, y2);
	LineTo(hdc, x1, y1);

	SelectObject(hdc, oldPen);
	DeleteObject(pen);

	SelectPalette(hdc, oldPal, FALSE);
#endif

	EndPaint(hWnd, &ps);

#ifdef DEBUG
	debugLog("-MainWindow::onPaint()\n");
#endif

}

void MainWindow::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
#ifdef DEBUG
	debugLog("MainWindow::onCommand(%d, %p, %d)\n", id, hwndCtl, codeNotify);
#endif

	if(installProcess != NULL)
		return;

	if(codeNotify == BN_CLICKED)
	{
		switch(id)
		{
		case BID_INSTALL:
			runInstallProgram();
			break;
		case BID_PLAY:
#if !defined(DISABLE_PLAY)
			// CreateProcess and then quit
			if(runGame())
				PostMessage(hWnd, WM_CLOSE, 0, 0);
#endif
			break;
		case BID_QUIT:
			PostMessage(hWnd, WM_CLOSE, 0, 0);
			break;
		case BID_README:
			WinExec("NOTEPAD README.TXT", SW_SHOW);
			break;
		case BID_UPDATES:
			WinExec("NOTEPAD UPDATES.TXT", SW_SHOW);
			break;
#ifdef DEBUG
		default:
			debugLog("Unknown id\n");
			break;
#endif
		}
	}
#ifdef DEBUG
	else
		debugLog("Unrecognised codeNotify\n");
#endif


}

/*
 * Message Handler
 */

LRESULT MainWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch(msg)
	{
		HANDLE_MSG(hWnd, WM_CREATE,			onCreate);
		HANDLE_MSG(hWnd, WM_DESTROY,			onDestroy);
		HANDLE_MSG(hWnd, WM_PAINT,				onPaint);
		HANDLE_MSG(hWnd, WM_CLOSE,				onClose);
		HANDLE_MSG(hWnd, WM_COMMAND,			onCommand);
		HANDLE_MSG(hWnd, WM_DRAWITEM,			onDrawItem);

		case WM_PALETTECHANGED:
		   if (wParam == (WPARAM)hWnd)            // Responding to own message.
				goto ignore;
			// Drop through
		HANDLE_MSG(hWnd, WM_QUERYNEWPALETTE, onQueryNewPalette);

	ignore:
	default:
		return defProc(hWnd, msg, wParam, lParam);
	}
}

/*
 * Add a button to the main window
 */

HWND MainWindow::addButton(HWND hwnd, int id, int x, int y, UINT rid)
{
	ASSERT(id >= 0);
	ASSERT(id < BID_HowMany);

	HWND hButton = CreateWindowEx(
		0,
		BUTTONCLASS, NULL,
		WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON | BS_OWNERDRAW,
		// WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON | BS_TEXT | BS_OWNERDRAWN,
		x, y, 128, 24,
		hwnd, (HMENU) id, gApp.getInstance(), NULL);
	ASSERT(hButton != NULL);

	char buffer[256];
	LoadString(gApp.getInstance(), rid, buffer, 256);
	SetWindowText(hButton, buffer);

	return hButton;
}


void MainWindow::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT * lpDrawItem)
{
#ifdef DEBUG
	debugLog("MainWindow::onDrawItem(%d, 0x%04x %d)\n",
		(int) lpDrawItem->CtlID,
		(int) lpDrawItem->itemState,
		(int) lpDrawItem->itemAction);
#endif

	// HDC hdcMem = gApp.getDibDC();

	HDC hdcMem = CreateCompatibleDC(lpDrawItem->hDC);

	ASSERT(hdcMem != NULL);

	ASSERT(btOn != NULL);
	ASSERT(btOff != NULL);
	ASSERT(btDisabled != NULL);

	HPALETTE hOldPal = SelectPalette(lpDrawItem->hDC, gApp.getPalette(), FALSE);
	RealizePalette(lpDrawItem->hDC);

	HBITMAP oldBM;

	COLORREF color;

	if(lpDrawItem->itemState & ODS_DISABLED)
	{
		oldBM = (HBITMAP) SelectObject(hdcMem, btDisabled);
		color = RGB(128, 128, 128);
	}
	else if(lpDrawItem->itemState & (ODS_SELECTED | ODS_CHECKED))
	{
		oldBM = (HBITMAP) SelectObject(hdcMem, btOn);
		color = RGB(255, 255, 255);
	}
	else
	{
		oldBM = (HBITMAP) SelectObject(hdcMem, btOff);
		color = RGB(255, 255, 180);
	}

	BitBlt(lpDrawItem->hDC,
		lpDrawItem->rcItem.left, lpDrawItem->rcItem.top,
		lpDrawItem->rcItem.right - lpDrawItem->rcItem.left,
		lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top,
		hdcMem, lpDrawItem->rcItem.left, lpDrawItem->rcItem.top,
		SRCCOPY);

	COLORREF oldColor = SetTextColor(lpDrawItem->hDC, color);
	char buffer[256];
	int tlen = GetWindowText(lpDrawItem->hwndItem, buffer, 256);
	SetBkMode(lpDrawItem->hDC, TRANSPARENT);
	DrawText(lpDrawItem->hDC, buffer, tlen, (LPRECT) &lpDrawItem->rcItem, DT_CENTER | DT_VCENTER);

	SetTextColor(lpDrawItem->hDC, oldColor);
	SelectObject(hdcMem, oldBM);
	SelectPalette(lpDrawItem->hDC, hOldPal, TRUE);
	RealizePalette(lpDrawItem->hDC);

	DeleteDC(hdcMem);
}


/*
 * Run the installation program
 */

void MainWindow::runInstallProgram()
{
#ifdef DEBUG
	debugLog("About to call CreateProcess\n");
#endif

	/*
	 * Execute the DOS installer
	 */

	PROCESS_INFORMATION procInfo = { 0 };

	STARTUPINFO startInfo = { 0 };

	startInfo.cb = sizeof(STARTUPINFO);
	startInfo.lpReserved = NULL; 
	startInfo.lpDesktop = NULL; 
	startInfo.lpTitle = NULL; 
	startInfo.dwX = 0; 
	startInfo.dwY = 0; 
	startInfo.dwXSize = 0; 
	startInfo.dwYSize = 0; 
	startInfo.dwXCountChars = 0; 
	startInfo.dwYCountChars = 0; 
	startInfo.dwFillAttribute = 0; 
	startInfo.dwFlags = 0; 
	startInfo.wShowWindow = 0; 
	startInfo.cbReserved2 = 0; 
	startInfo.lpReserved2 = NULL; 
	startInfo.hStdInput = NULL; 
	startInfo.hStdOutput = NULL; 
	startInfo.hStdError = NULL; 

	char fname[MAX_PATH + 1];
	char path[MAX_PATH + 1];

	if(GetCurrentDirectory(MAX_PATH, path) == 0)
	{
#ifdef DEBUG
		DWORD err = GetLastError();
		debugLog("GetCurrentDirectory Failed... error=%ld\n", err);
#endif
		return;

	}

	/*
	 * Remove \ from end of path if it is there
	 */
#if 0
	size_t l = lstrlen(path);
	if(l)
	{
		l--;
		if(path[l] == '\\')
			path[l] = 0;
	}

	lstrcat(path, "\\INSTALL");

	lstrcpy(fname, path);
	lstrcat(fname, "\\INSTALL.EXE");
#else
	lstrcpy(fname, path);

	size_t l = lstrlen(fname);
	if(l)
	{
		l--;
		if(fname[l] == '\\')
			fname[l] = 0;
	}

	lstrcat(fname, "\\INSTALL.EXE");
#endif

#ifdef DEBUG
	debugLog("currentpath=%s\n", path);
	debugLog("fname=%s\n", fname);
#endif

	BOOL result = CreateProcess(
						// "INSTALL.EXE", 			// appName
						// "", 							// cmdLine
						NULL, fname,

						NULL, 						// ProcAttributes
						NULL,							// Security Attributes
						FALSE,						// InheritFlags
						NORMAL_PRIORITY_CLASS,	// CreationFlags
						NULL,							// Environment
						path,							// Current directory
						&startInfo,					// Startup info
						&procInfo);					// Process Information

	if(!result)
	{
#ifdef DEBUG
		DWORD err = GetLastError();
		debugLog("Failed... error=%ld\n", err);
#endif
		return;
	}

#ifdef DEBUG
	debugLog("Process Handle=%p\n", procInfo.hProcess);
#endif

	/*
	 * Wait until Install is finished
	 */

	WaitForSingleObject(procInfo.hProcess, INFINITE);

#if 0
	DWORD exitCode;

	while(GetExitCodeProcess(procInfo.hProcess, &exitCode) &&
			(exitCode == STILL_ACTIVE) )
	{
		Sleep(100);			// Sleep for 100 ms.
	}
#endif

#ifdef DEBUG
	debugLog("Install Finished\n");
#endif

	/*
	 * Find out where it installed to (how?)
	 */

	/*
	 * Set up some program groups and add information to registry
	 */

	/*
	 * Enable Play button
	 */

}



/*
 * Run the installation program
 * return TRUE if started
 */

BOOL MainWindow::runGame()
{
#ifdef DEBUG
	debugLog("About to call CreateProcess\n");
#endif

	/*
	 * Execute the DOS installer
	 */

	PROCESS_INFORMATION procInfo = { 0 };

	STARTUPINFO startInfo = { 0 };

	startInfo.cb = sizeof(STARTUPINFO);
	startInfo.lpReserved = NULL; 
	startInfo.lpDesktop = NULL; 
	startInfo.lpTitle = NULL; 
	startInfo.dwX = 0; 
	startInfo.dwY = 0; 
	startInfo.dwXSize = 0; 
	startInfo.dwYSize = 0; 
	startInfo.dwXCountChars = 0; 
	startInfo.dwYCountChars = 0; 
	startInfo.dwFillAttribute = 0; 
	startInfo.dwFlags = 0; 
	startInfo.wShowWindow = 0; 
	startInfo.cbReserved2 = 0; 
	startInfo.lpReserved2 = NULL; 
	startInfo.hStdInput = NULL; 
	startInfo.hStdOutput = NULL; 
	startInfo.hStdError = NULL; 

	char fname[MAX_PATH + 1];
	char path[MAX_PATH + 1];

	if(gameDir == 0)
	{
		if(GetCurrentDirectory(MAX_PATH, path) == 0)
		{
#ifdef DEBUG
			DWORD err = GetLastError();
			debugLog("GetCurrentDirectory Failed... error=%ld\n", err);
#endif
			return FALSE;
		}
	}
	else
		lstrcpy(path, gameDir);

	lstrcpy(fname, path);
	lstrcat(fname, "\\CIVIL.BAT");

#ifdef DEBUG
	debugLog("currentpath=%s\n", path);
	debugLog("fname=%s\n", fname);
#endif

	BOOL result = CreateProcess(
						// "INSTALL.EXE", 			// appName
						// "", 							// cmdLine
						NULL, fname,

						NULL, 						// ProcAttributes
						NULL,							// Security Attributes
						FALSE,						// InheritFlags
						NORMAL_PRIORITY_CLASS,	// CreationFlags
						NULL,							// Environment
						path,							// Current directory
						&startInfo,					// Startup info
						&procInfo);					// Process Information

	if(!result)
	{
#ifdef DEBUG
		DWORD err = GetLastError();
		debugLog("Failed... error=%ld\n", err);
#endif
		return FALSE;
	}

	return TRUE;
}



#if !defined(DISABLE_PLAY)
/*
 * Check registry for the game directory
 */

void MainWindow::getPlayDirectory()
{
	EnableWindow(playButton, (gameDir != 0));
}
#endif
