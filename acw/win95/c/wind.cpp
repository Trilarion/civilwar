/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Base Class for handling Windows call back functions
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 14:08:48  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 * Revision 1.6  1996/02/22 10:28:36  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1996/02/15 10:50:34  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1995/11/22 10:43:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1995/11/13 11:08:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1995/10/18 10:59:19  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1995/10/16 11:31:51  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "globals.hpp"
#include "wind.hpp"

/*
 * Some useful values
 */

const char BUTTONCLASS[] = "BUTTON";
const char STATICCLASS[] = "STATIC";
const char MDICLIENTCLASS[] = "MDICLIENT";

/*
 * Base Constructor
 */

WindowBase::WindowBase()
{
	hWnd = NULL;
}

/*
 * Window CallBack function for any window derived from WindowBase
 */

LRESULT CALLBACK WindowBase::baseWindProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#ifdef DEBUG_MESSAGES
	debugLog("WindowBase::baseWindProc(%p, %d, %d, %d)\n",
		hWnd,
		msg,
		wParam,
		lParam);
#endif

	WindowBase* wBase = (WindowBase*) GetWindowLong(hWnd, 0);

	/*
	 * Try and retrieve the class's address from the WC_NCCREATE
	 * message.
	 */

	if(wBase == 0)
	{
		if(msg == WM_NCCREATE)
		{
			LPCREATESTRUCT lpcs = (LPCREATESTRUCT) lParam;
			wBase = (WindowBase*) lpcs->lpCreateParams;

			if(wBase == NULL)
				return FALSE;

			wBase->hWnd = hWnd;
			SetWindowLong(hWnd, 0, (LONG) wBase);

			return TRUE;
		}
		else
			return DefWindowProc(hWnd, msg, wParam, lParam);
	}

	if(msg == WM_NCDESTROY)
	{
		delete wBase;
		wBase = NULL;
		SetWindowLong(hWnd, 0, (LONG) wBase);
		return 0;
	}

#if 0
	/*
	 * Handle problem of activating window and subsequent button press
	 *
	 * Ignore button press that immediately follows WM_MOUSEACTIVATE
	 */

	else if(msg == WM_MOUSEACTIVATE)
	{
		debugLog("WM_MOUSEACTIVATE: %08x\n", (ULONG) hWnd);

		HWND hwndTop = (HWND) wParam;

		if(hwndTop == hWnd)
			return MA_ACTIVATEANDEAT;
	}
#endif

	/*
	 * If class already set up, then process messages
	 */

	return wBase->procMessage(hWnd, msg, wParam, lParam);
}

/*
 * Default message processor for WindowBase
 */

virtual LRESULT WindowBase::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	return DefWindowProc(hWnd, msg, wParam, lParam);
}


/*
 * Child Window Class
 */

int ChildWindow::onMouseActivate(HWND hwnd, HWND hwndTopLevel, UINT codeHitTest, UINT msg)
{

	SetWindowPos(hwnd, HWND_TOP, 0,0,0,0, SWP_NOMOVE | SWP_NOSIZE);

	return MA_ACTIVATE;
}

/*
 * Help Function to set a window's size
 */

void alterWindowSize(HWND hWnd, LONG lWidth, LONG lHeight, CREATESTRUCT* lpCreateStruct)
{
	RECT r;
	GetClientRect(hWnd, &r);

	r.right = r.left + lWidth;
	r.bottom = r.top + lHeight;

	AdjustWindowRectEx(&r, lpCreateStruct->style, FALSE, lpCreateStruct->dwExStyle);

	int w = r.right - r.left;
	int h = r.bottom - r.top;

	SetWindowPos(hWnd, HWND_TOP, 0, 0, w, h, SWP_NOMOVE | SWP_NOZORDER);
}


/*
 * Popup Window base Class
 */

static const char PopupWindow::className[] = "PopupWindow";

static ATOM PopupWindow::classAtom = 0;

static ATOM PopupWindow::registerClass(HINSTANCE hinst)
{
	if(!classAtom)
	{
		WNDCLASS wc;

		wc.style = CS_OWNDC | CS_DBLCLKS;
		wc.lpfnWndProc = baseWindProc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = sizeof(PopupWindow*);
		wc.hInstance = hinst;
		wc.hIcon = NULL;
		wc.hCursor = LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground = (HBRUSH) (1 + COLOR_3DFACE);
		wc.lpszMenuName = NULL;
		wc.lpszClassName = getClassName();
		classAtom = RegisterClass(&wc);
	}

	ASSERT(classAtom != 0);

	return classAtom;
}
