/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Implementation of Generic Application Class
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 14:08:48  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 * Revision 1.3  1996/06/22 19:06:06  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1995/11/07 10:39:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1995/10/16 11:31:51  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "globals.hpp"


BOOL APP::init(HINSTANCE inst, HINSTANCE prev, LPSTR cmd, int show)
{
	hinst = inst;
	lpCmdLine = cmd;
	nCmdShow = show;

	if(!prev)
		if(!initApplication())
			return FALSE;

	if(!doCmdLine(cmd))
		return FALSE;

	if(!initInstance())
		return FALSE;

	return TRUE;
}



