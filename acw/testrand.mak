#####################################################################
#
# Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
# This Software is subject to the GNU General Public License.  
# For License information see the file COPYING in the root directory of the project.
# For more information see the file README.
#
#####################################################################

!include include.mak

all: testrand.exe testrg.exe  .SYMBOLIC

RANDOBS = testrand.obj random.obj 

testrand.exe: $(RANDOBS)
	@%make testrand.lnk
	$(LN) $(LNFLAGS) NAME $@ @$(LNK)\testrand.lnk

testrand.lnk : testrand.mak
	@%create $(LNK)\$^.
	@%append $(LNK)\$^. PATH $(O)
	@for %i in ($(RANDOBS)) do @%append $(LNK)\$^. FILE %i

rgOBS = testrg.obj random.obj 

testrg.exe: $(rgOBS)
	@%make testrg.lnk
	$(LN) $(LNFLAGS) NAME $@ @$(LNK)\testrg.lnk

testrg.lnk : testrand.mak
	@%create $(LNK)\$^.
	@%append $(LNK)\$^. PATH $(O)
	@for %i in ($(rgOBS)) do @%append $(LNK)\$^. FILE %i

