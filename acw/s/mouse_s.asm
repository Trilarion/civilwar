;---------------------------------------------------------------------
; Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
; This Software is subject to the GNU General Public License.  
; For License information see the file COPYING in the root directory of the project.
; For more information see the file README.
;
;---------------------------------------------------------------------
; $Id$
;----------------------------------------------------------------------
;
; Mouse Interface
;
;----------------------------------------------------------------------
;
; $Log$
; Revision 1.1  2001/03/15 15:18:13  greenius
; Renamed files to lower case
;
; Revision 1.1  2001/03/11 00:58:50  greenius
; Added to sourceforge
;
; Revision 1.4  1994/07/11  14:32:53  Steven_Green
; *** empty log message ***
;
; Revision 1.3  1994/05/04  22:13:50  Steven_Green
; *** empty log message ***
;
; Revision 1.2  1993/11/09  14:03:37  Steven_Green
; far pointer passed to install function
;
; Revision 1.1  1993/11/05  16:53:45  Steven_Green
; Initial revision
;
;
;----------------------------------------------------------------------

	include "types.i"
	include "mouse_s.i"

MOUSE_UNDER_TIMER = 0		; Set this for timer based
MOUSE_CALLBACK = 1			; Set this if using callback function


 if 0
;------------------------------------------
; WORD initMouse()
;
; Initialise Mouse
;
; Returns 0: Mouse driver not installed
;         -1 Mouse driver is installed

	proc initMouse_
		enter 0,0
		push bx

		xor ax,ax
		int 033h

		pop bx
		leave
		ret
	endp initMouse_

;------------------------------------------
; void setMouseHandler(UWORD event:ax, far (function*)():cx,ebx)
;
; Install Mouse handler Interrupt
;
; Note that the address is a protected mode address!
;
; Set up the handler to call _mouseInterrupt which will
; set up a stack and segment registers before calling
; the provided routine.


	public mouseStack,_mouseStack,mouseHandler,oldMouseSS,topMouseStack,_mouseInterrupt


	UDATASEG

mouseStack	db 1024 dup(?)		; A stack used by the mouse handler
_mouseStack:

mouseHandler farPtr ?
oldMouseSS  farPtr ?		; Value to store ss:esp
; mouseAX dw ?

	DATASEG

topMouseStack dp _mouseStack
mouseLock db 0				; Set if inside the interrupt routine

	CODESEG


	proc _mouseInterrupt far
		push es ds

			push ax
				mov ax,DGROUP
				mov ds,ax
				mov es,ax
			pop ax

			; Check if alreay inside this interrupt!  Ignore if we are
			; otherwise we'll overwrite our stack

			inc [mouseLock]
			cmp [mouseLock],1
			jnz @@locked

			; Set up new stack

			; mov [mouseAX],ax
			
			mov [oldMouseSS.off],esp
			mov [oldMouseSS.seg],ss

			lss esp,[topMouseStack]
			; mov ax,seg topMouseStack
			; mov ss,ax
			; mov esp,offset topMouseStack

			; mov ax,[mouseAX]

			; Save remaining registers

			push eax ebx ecx edx esi edi
		
			; Call routine

				call [mouseHandler.pointer]

			; Restore registers

			pop edi esi edx ecx ebx eax

		; Restore Old Stack

		lss esp,[oldMouseSS.pointer]

		dec [mouseLock]
@@locked:
		pop ds es

		ret

	endp _mouseInterrupt


	proc setMouseHandler_
		enter 0,0
		push cx dx es

		mov [mouseHandler.off],ebx
		mov [mouseHandler.seg],cx

		mov cx,ax			; event in cx

		mov ax,seg _mouseInterrupt				; Function address in es:edx
		mov es,ax
		mov edx,offset _mouseInterrupt

		mov ax,12

		int 033h

		pop es dx cx
		leave
		ret
	endp setMouseHandler_

;------------------------------------
; setMouseRange(short x, short y, short w, short h)
;
; x : ax
; y : dx
; w : bx
; h : cx

	proc setMouseRange_
	LOCAL minY:WORD, maxY:WORD
	enter 4,0

		push bx cx dx

		add bx,ax			; maxX = x + w - 1
		dec bx
		add cx,dx			; maxY = y + h - 1
		dec cx

		mov [minY],dx		; Save Y parameters
		mov [maxY],cx

		mov cx,ax			; Setup X parameters... CX = Min
		mov dx,bx			; DX = Max
		mov ax,7
		int 033h

		mov cx,[minY]		; Setup Y parameters
		mov dx,[maxY]
		mov ax,8
		int 033h

		pop dx cx bx

	leave
	ret
	endp setMouseRange_

;------------------------------------
; setMousePosition(short x, short y)
;
; x : cx	; (ax)
; y : dx

	proc setMousePosition_
	enter 0,0

		push cx dx

		; mov cx,ax			; CX = X
		; dx already set
		mov ax,4
		int 033h

		pop dx cx

	leave
	ret
	endp setMousePosition_


;------------------------------------
; setMouseRatio(short x, short y)
;
; x : cx
; y : dx

	proc setMouseRatio_
	enter 0,0

		push cx dx

		mov ax,0fh
		int 033h

		pop dx cx

	leave
	ret
	endp setMouseRatio_


 endif	; 0



 if MOUSE_UNDER_TIMER


	UDATASEG

mouseStack	db 2048 dup(?)		; A stack used by the mouse handler
_mouseStack:

mouseHandler farPtr ?
oldMouseSS  farPtr ?		; Value to store ss:esp

	DATASEG

topMouseStack dp _mouseStack
mouseLock db 0				; Set if inside the interrupt routine

	CODESEG

	extrn mouseHandler_:proc

	proc mouseInterrupt_ far
		pushad
		push ds es fs gs

			mov ax,DGROUP
			mov ds,ax
			mov es,ax

			; Check if alreay inside this interrupt!  Ignore if we are
			; otherwise we'll overwrite our stack

			inc [mouseLock]
			cmp [mouseLock],1
			jnz @@locked

			; Set up new stack
			
			mov [oldMouseSS.off],esp
			mov [oldMouseSS.seg],ss

			lss esp,[topMouseStack]

		  	call mouseHandler_

		; Restore Old Stack

		lss esp,[oldMouseSS.pointer]

@@locked:
		dec [mouseLock]

		pop gs fs es ds
		popad
		iretd

	endp mouseInterrupt_

 else

;-----------------------------------------------
; Callback version




	UDATASEG

mouseStack	db 1024 dup(?)		; A stack used by the mouse handler
_mouseStack:

oldMouseSS  farPtr ?		; Value to store ss:esp

	DATASEG

topMouseStack dp _mouseStack
mouseLock db 0				; Set if inside the interrupt routine

	CODESEG

	extrn mouseHandler_:proc

	proc mouseInterrupt_ far
	  push ds es
		 push ax
			mov ax,DGROUP
			mov ds,ax
			mov es,ax
		 pop ax

			; Check if alreay inside this interrupt!  Ignore if we are
			; otherwise we'll overwrite our stack

			inc [mouseLock]
			cmp [mouseLock],1
			jnz @@locked

			; Set up new stack
			
			mov [oldMouseSS.off],esp
			mov [oldMouseSS.seg],ss

			lss esp,[topMouseStack]

			pushad
		  	call mouseHandler_
			popad

		; Restore Old Stack

		lss esp,[oldMouseSS.pointer]

@@locked:
		dec [mouseLock]

		pop es ds
		ret

	endp mouseInterrupt_

	proc setMouseHandler_
		push es
		push ecx edx
		mov ax,seg mouseInterrupt_
		mov es,ax
		mov edx,offset mouseInterrupt_
		mov cx,01fh
		mov ax,12
		int 033h
		pop edx ecx
		pop es
		ret
	endp setMouseHandler_



 endif



	end
