;---------------------------------------------------------------------
; Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
; This Software is subject to the GNU General Public License.  
; For License information see the file COPYING in the root directory of the project.
; For more information see the file README.
;
;/************************************************************************
; *
; *     File        :   NOPLAYER.ASM
; *     Description :   Null Sound device for use with DSMI
; *
; *     Copyright (C) 1994, Steven Green
; *
; ***********************************************************************
;
;       Revision history of MCPLAYER.ASM
;
; ***********************************************************************/

	IDEAL
	JUMPS
	P386

	INCLUDE "MODEL.INC"
	INCLUDE "CDI.INC"

CSEGMENTS NOPLAYER

CDATASEG
ENDS

CCODESEG NOPLAYER

CPROC   nullFunction

        sub     _ax,_ax
        sub     _dx,_dx
	ret
ENDP

CPROC   nsGetDelta
		  mov _ax,10000		; 10 ms?
        sub _dx,_dx
	ret
ENDP



ENDS

CDATASEG

	CPUBLIC CDI_NOSOUND

	_CDI_NOSOUND         CDIDEVICE <\
		_far ptr _nullFunction,\
		_far ptr _nullFunction,\
		_far ptr _nullFunction,\
		_far ptr _nullFunction,\
									  \
		_far ptr _nullFunction,\
		_far ptr _nullFunction,\
		_far ptr _nullFunction,\
		_far ptr _nullFunction,\
									  \
		_far ptr _nullFunction,\
		_far ptr _nullFunction,\
		_far ptr _nullFunction,\
		_far ptr _nullFunction,\
									  \
		_far ptr _nullFunction,\
		_far ptr _nullFunction,\
		_far ptr _nsGetDelta,  \
		_far ptr _nullFunction,\
      _far ptr _nullFunction,\
      _far ptr _nullFunction,\
									  \
      _far ptr _nullFunction,\
		_far ptr _nullFunction,\
		_far ptr _nullFunction,\
		_far ptr _nullFunction,\
		_far ptr _nullFunction,\
		_far ptr _nullFunction,\
		_far ptr _nullFunction>

ENDS

END

