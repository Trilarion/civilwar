/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *====================================================
 *
 * Sprite Definitions for icons.lst
 *
 *====================================================
 */

#define SPR_mice 0
#define SPR_icon_1 11
#define SPR_icon_2 28
#define SPR_icon_3 57
#define SPR_cityinfo 80
#define SPR_armyinfo 125
#define SPR_cpsm1rc2 135
#define SPR_scroller 154
#define SPR_calicon 182
#define SPR_flags 204
#define SPR_clock 224
#define SPR_submenu 229
#define SPR_fill 232
/* Total number of sprites = 233 */
