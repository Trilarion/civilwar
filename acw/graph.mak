#####################################################################
#
# Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
# This Software is subject to the GNU General Public License.  
# For License information see the file COPYING in the root directory of the project.
# For more information see the file README.
#
#####################################################################
# $Id$
##################################
# Process Graphics for Dagger Wargame
#
# Invoke with:
#   maker -fgraph.mak
##################################
#
# $Log$
# Revision 1.1  2001/03/15 14:08:48  greenius
# Converted filenames to lower case
#
# Revision 1.1  2001/03/11 00:58:48  greenius
# Added to sourceforge
#
#
# Revision 1.16  1994/06/09  23:40:42  Steven_Green
# Emma's new fonts added
#
# Revision 1.13  1994/04/20  22:26:36  Steven_Green
# Added font grabber
#
# Revision 1.5  1994/02/10  14:45:32  Steven_Green
# Added CAL Icons
#
# Revision 1.1  1994/01/11  21:15:19  Steven_Green
# Initial revision
#
#
##################################

.extensions
.extensions: .spr .fnt .lbm .bbm .lst

##################################
# Flags

SPRFLAGS = -vq -a238 -s -c

##################################
# Paths

.spr : art
.fnt : art
.lbm : artsrc
.bbm : artsrc
# .lst : artsrc

CP = !fcopy

##################################
# Default rules

.lbm.spr:
	getspr $(SPRFLAGS) $[* art\$^@
        $(CP) art\$^@

.bbm.spr:
	getspr $(SPRFLAGS) $[* art\$^@
        $(CP) art\$^@

.lbm.fnt:
         getspr -vv -f $[* $^@
         $(CP) $@

##################################
# What we're making

BIGMAP = art\maps\mapzoom.bm
TERRMAP = art\terrmap.ter

all: art\mice.spr art\system.spr art\game.spr art\battle.spr &
	art\f_jame_5.fnt &
	art\f_jame08.fnt &
	art\f_jame15.fnt &
	art\f_jame32.fnt &
        art\f_emfl_8.fnt art\f_emfl10.fnt art\f_emfl15.fnt art\f_emfl32.fnt &
	art\f_emma14.fnt &
	art\texture.spr &
	$(BIGMAP) &
	$(TERRMAP)

##################################
# Terrain Map

$(TERRMAP) : terrmap.lbm
	terrconv $< $^@
	$(CP) $^@


##################################
# Explicit commands

##################################
# Mouse Pointers

MICESPR = mice.lbm

art\mice.spr: $(MICESPR)
	@%make mice.lst
	getspr $(SPRFLAGS) -ilibh\micespr.h @mice.lst
	move mice.spr art
        $(CP) $^@

mice.lst : graph.mak
	@%create $^@
	@for %i in ($(MICESPR)) do @%append $^@ artsrc\%i

##################################
# System Sprites

SYSSPR = scroller.lbm sysfill.lbm

art\system.spr: $(SYSSPR)
	@%make system.lst
	getspr $(SPRFLAGS) -ilibh\sys_spr.h @system.lst
	move system.spr art
        $(CP) $^@

system.lst : graph.mak
	@%create $^@
	@for %i in ($(SYSSPR)) do @%append $^@ artsrc\%i


##################################
# General Game graphics

gameSPR = &
	icon_1.lbm 	&
	icon_2.lbm 	&
	icon_3.lbm	&
	cityinfo.lbm	&
	armyinfo.lbm	&
	calicon.lbm	&
	cpsm1rc2.lbm	&
	flags.lbm	&
	clock.lbm	&
	submenu.lbm	&
	fill.lbm

art\game.spr: $(gameSPR)
	@%make game.lst
	getspr $(SPRFLAGS) -ih\gamespr.h @game.lst
	move game.spr art
        $(CP) $^@

game.lst : graph.mak
	@%create $^@
	@for %i in ($(gameSPR)) do @%append $^@ artsrc\%i

##################################
# Battle Game

BATGRAPH = batflag.lbm &
	i_reload.lbm	&
	i_shoot.lbm	&
	i_walk.lbm	&
	i_charge.lbm	&
	i_stand.lbm	&
	horses.lbm    	&
	stnd_hrs.lbm	&
	cavalry.lbm	&
	mt_infnt.lbm	&
	limbers.lbm  	&
	canons.lbm     	&
	c_fire_1.lbm   	&
	gunfire1.lbm   	&
	deadgun.lbm	&
	deadmanc.lbm	&
	deadmanu.lbm	&
	dedhorse.lbm	&
	earth.lbm      &
	sky.lbm	       &
	batzoom.lbm    &
	trees.lbm      &
	church1.lbm    &
	church1d.lbm   &
	chapel_1.lbm   &
	house01.lbm    &
	house1d.lbm    &
	house11.lbm    &
	tavern1.lbm    &
	tavern1d.lbm   &
	tavern11.lbm   &
	tent.lbm       &
	silly.lbm

####
#	reloadu1.lbm	&
#	shootrc1.lbm	&
#	walkusa2.lbm	&
#	walkcsa2.lbm	&
#	charusa2.lbm	&
#	char_csa.lbm	&
#	stand2.lbm	&
#	cavalryu.lbm   &
#	m_inf_u1.lbm   &
#	cavalryc.lbm   &
#	m_inf_c1.lbm   &

####
#	stand_u.lbm    &
#	walk_usa.lbm   &
#	char_usa.lbm   &
#	shooter.lbm    &
#	reload_u.lbm   &
#	stand_c.lbm    &
#	walk_csa.lbm   &
#	char_csa.lbm   &
#	shooterc.lbm   &
#	reload_c.lbm   &

art\battle.spr : $(BATGRAPH) # graph.mak
	@%make battle.lst
	getspr $(SPRFLAGS) -ih\batspr.h @battle.lst
	move battle.spr art
	$(CP) $^@

battle.lst : graph.mak
	@%create $^@
	@for %i in ($(BATGRAPH)) do @%append $^@ artsrc\%i

##################################
# Big Map

MAPDIR = artsrc\maps

$(BIGMAP) : $(MAPDIR)\hugemap.lbm
	makemap $< $^@
	$(CP) $^@

$(MAPDIR)\hugemap.lbm : $(MAPDIR)\map1a.lbm &
			$(MAPDIR)\map1b.lbm &
			$(MAPDIR)\map1c.lbm &
			$(MAPDIR)\map1d.lbm &
			$(MAPDIR)\map1e.lbm &
			$(MAPDIR)\map2a.lbm &
			$(MAPDIR)\map2b.lbm &
			$(MAPDIR)\map2c.lbm &
			$(MAPDIR)\map2d.lbm &
			$(MAPDIR)\map2e.lbm &
			$(MAPDIR)\map3a.lbm &
			$(MAPDIR)\map3b.lbm &
			$(MAPDIR)\map3c.lbm &
			$(MAPDIR)\map3d.lbm &
			$(MAPDIR)\map3e.lbm &
			$(MAPDIR)\map4a.lbm &
			$(MAPDIR)\map4b.lbm &
			$(MAPDIR)\map4c.lbm &
			$(MAPDIR)\map4d.lbm &
			$(MAPDIR)\map4e.lbm &
			$(MAPDIR)\map5a.lbm &
			$(MAPDIR)\map5b.lbm &
			$(MAPDIR)\map5c.lbm &
			$(MAPDIR)\map5d.lbm &
			$(MAPDIR)\map5e.lbm
	@%create maptmp.bat
	@%append maptmp.bat pushd $(MAPDIR)
        @%append maptmp.bat call hugemap
	@%append maptmp.bat popd
	call maptmp.bat
	del maptmp.bat

