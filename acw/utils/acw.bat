rem Setup for developing Civil War

set HOME=F:\CivilWar
set WD=/tr#win/nog
set DSMI_LIB=f:\CivilWar\dsmi\lib32
set DSMI_H=f:\CivilWar\dsmi\include
set wcl386= /4r /d2 /i#%HOME\h -i#%HOME\libh -i#%DSMI_H /s /zm /DDEBUG /DTESTING /D__C32__
inspath %WATCOM%\BINW
inspath %WATCOM%\BINNT
addpath %HOME%\UTILS
addpath C:\RCS\BINDOS
alias m wmake
alias mdb `wmake "NODEBUG="`
alias mnl wmake "NOLOG="
alias med wmake -f editor.mak
alias mbat wmake -f batedit.mak
alias fcopy `*copy %1 l:%1`
alias wd %WATCOM%\binw\wd

