setlocal

set CD_DIR=acw_cd
call makedir %CD_DIR > nul:

*copy /u CIVIL.BAT %CD_DIR
*copy /u CIVILWAR.BAT %CD_DIR
*copy /u AM.BAT %CD_DIR
*copy /u AMERIKA.BAT %CD_DIR
*copy /u SETUPSND.BAT %CD_DIR
*copy /u DOS4GW.EXE %CD_DIR
*copy /u text\readme\*.* %CD_DIR

call makedir %CD_DIR\ANIM > nul:
*copy /u ANIM\6LB_GUN.FLI %CD_DIR\anim
*copy /u ANIM\12LB_NAP.FLI %CD_DIR\ANIM
*copy /u ANIM\BOAT2.FLI %CD_DIR\ANIM
*copy /u ANIM\BOAT4.FLI %CD_DIR\ANIM
*copy /u ANIM\CANON2.FLI %CD_DIR\ANIM
*copy /u ANIM\RIFLE1.FLI %CD_DIR\ANIM
*copy /u ANIM\SHARPS.FLI %CD_DIR\ANIM

call makedir %CD_DIR\ART > nul:
*copy /us art %CD_DIR\art

call makedir %CD_DIR\DBDATA > nul:
*copy /u dbdata %CD_DIR\dbdata

call makedir %CD_DIR\EXE > nul:
*copy /u exe\DWG.EXE %CD_DIR\exe
*copy /u exe\DWG_4M.EXE %CD_DIR\exe
*copy /u exe\HASMEM.EXE %CD_DIR\exe
*copy /u exe\RUNDB.EXE %CD_DIR\exe
*copy /u exe\TESTMUS.EXE %CD_DIR\exe
*copy /u exe\VGAFLIC.EXE %CD_DIR\exe

call makedir %CD_DIR\GAMEDATA > nul:
*copy /u gamedata\START.CAM %CD_DIR\gamedata
*copy /u gamedata\BULLRUN.BTL %CD_DIR\gamedata
*copy /u gamedata\MILLSPR.BTL %CD_DIR\gamedata
*copy /u gamedata\SHILOH.BTL %CD_DIR\gamedata
*copy /u gamedata\WILSONS.BTL %CD_DIR\gamedata
*copy /u gamedata\antietam.btl %CD_DIR\gamedata
*copy /u gamedata\chickamu.btl %CD_DIR\gamedata
*copy /u gamedata\fredrick.btl %CD_DIR\gamedata
*copy /u gamedata\gettysbg.btl %CD_DIR\gamedata

call makedir %CD_DIR\SOUNDS > nul:
*copy /u sounds\CANNON.WAV %CD_DIR\SOUNDS
*copy /u sounds\MELEE.WAV %CD_DIR\SOUNDS
*copy /u sounds\SHOTGUN.WAV %CD_DIR\SOUNDS

call makedir %CD_DIR\INTRO > nul:
*copy /us intro %CD_DIR\art

*copy win95\autorun.exe %CD_DIR
*copy win95\autorun.inf %CD_DIR

for %i in ( %CD_DIR\exe\*.exe ) do wstrip %i
wstrip %CD_DIR\autorun.exe


