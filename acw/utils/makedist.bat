set DIST=acw_cd
copy /u CIVIL.BAT dist
copy /u CIVILWAR.BAT dist
copy /u DOS4GW.EXE dist
copy /u text\readme\*.* dist
copy /u SETUPSND.BAT dist
copy /u ANIM\6LB_GUN.FLI dist\anim
copy /u ANIM\12LB_NAP.FLI dist\ANIM
copy /u ANIM\BOAT2.FLI dist\ANIM
copy /u ANIM\BOAT4.FLI dist\ANIM
copy /u ANIM\CANON2.FLI dist\ANIM
copy /u ANIM\RIFLE1.FLI dist\ANIM
copy /u ANIM\SHARPS.FLI dist\ANIM
copy /us art dist\art
copy /u dbdata dist\dbdata
copy /u exe\DWG.EXE dist\exe
copy /u exe\DWG_4M.EXE dist\exe
copy /u exe\HASMEM.EXE dist\exe
copy /u exe\RUNDB.EXE dist\exe
copy /u exe\TESTMUS.EXE dist\exe
copy /u exe\VGAFLIC.EXE dist\exe
copy /u gamedata\START.CAM dist\gamedata
copy /u gamedata\BULLRUN.BTL dist\gamedata
copy /u gamedata\MILLSPR.BTL dist\gamedata
copy /u gamedata\SHILOH.BTL dist\gamedata
copy /u gamedata\WILSONS.BTL dist\gamedata
copy /u sounds\CANNON.WAV dist\SOUNDS
copy /u sounds\MELEE.WAV dist\SOUNDS
copy /u sounds\SHOTGUN.WAV dist\SOUNDS
for %i in ( dist\exe\*.exe ) do wstrip %i

