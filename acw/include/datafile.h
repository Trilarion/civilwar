/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef DATAFILE_H
#define DATAFILE_H

#ifndef __cplusplus
#error datafile.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Data files used in game
 *
 * Initially all the data files will be stored as ASCII using keywords
 * so that they can be easily editted and examined.
 *
 * Later in the project, text to binary conversion may be done to
 * reduce file size and speed up reading.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.4  1994/09/23  13:29:31  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/07/25  20:34:58  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/06/24  14:45:10  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/06/09  23:36:46  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"

class CampaignWorld;

void writeWorld(const char* fileName, CampaignWorld* world);
void  readWorld(const char* fileName, CampaignWorld* world);


/*
 * Keyword Id's, must match up with the text above!
 */

enum KeyWords {
	KW_WorldFile,

	KW_StateSection,
	KW_FacilitySection,
	// KW_SeaZones,
	// KW_RiverZones,
	KW_WaterNetwork,

	KW_Facility,
	// KW_NamedFacility,
	KW_City,

	KW_State,
	KW_Flags,		// This is redundant and will be removed soon
	KW_Side,
	KW_USA,
	KW_CSA,

	KW_Fortification,
	KW_Wagons,
	KW_RailCapacity,
	KW_RailLink,
	KW_Name,
	KW_KeyCity,
	KW_Type,
	KW_Size,
	KW_Resources,

	KW_Depot,
	KW_Recruit,
	KW_Training,
	KW_Hospital,
	KW_POW,
	KW_Capital,
	KW_RailEngineer,

	KW_WaterCapacity,
	KW_Blockade,
	KW_Victory,

	KW_WaterZone,
	KW_WaterLink,

	KW_Sea,
	KW_Coast,
	KW_River,
	KW_Facilities,

	KW_NavalUnit,
	KW_IronClad,
	KW_Riverine,
	KW_Monitor,

	KW_Location,

	/*
	 * Order of Battle
	 */

	KW_OBSection,
	KW_General,
	KW_President,
	KW_Army,
	KW_Corps,
	KW_Division,
	KW_Brigade,
	KW_Regiment,
	KW_Unit,

	KW_Rank,
	KW_Strength,
	KW_Stragglers,
	KW_Casualties,
	KW_Fatigue,
	KW_Morale,
	KW_Speed,
	KW_Supply,
	KW_Experience,
	// KW_MoveTime,
	KW_Time,
	KW_Order,
	KW_SentOrder,
	KW_OrderList,

	KW_Efficiency,
	KW_Ability,
	KW_Aggression,

	KW_OrderHow,
	KW_OrderLand = KW_OrderHow,
	KW_OrderRail,
	KW_OrderWater,

	KW_OrderMode,
	KW_CautiousAdvance = KW_OrderMode,
	KW_Advance,
	KW_Attack,

	KW_Stand,
	KW_Hold,
	KW_Defend,

	KW_Withdraw,
	KW_Retreat,
	KW_HastyRetreat,

	KW_Occupying,
	KW_Sieging,

	/*
	 * End of List... how many there are!
	 */

	KW_Last,

	/*
	 * Special values for errors, etc
	 */

	KW_NotFound,		// Unknown keyword
	KW_Error,			// File error
	KW_EndFile,			// End of File

};

/*
 * A sorted list is used for quick parsing using binary search
 */

struct KeywordItem {
	const char* word;
	KeyWords id;
};

class KeywordList {
	KeywordItem* items;
	int howMany;
public:
	KeywordList();
	void create(const char** words, int count);
	~KeywordList();

	KeyWords find(const char* s);
};

extern KeywordList keys;
extern const char* keywords[];


/*
 * Constants
 */

const char quote = '\"';
const UWORD WorldVersion = 0x0000;
const char startChunk = '{';
const char endChunk = '}';


#endif /* DATAFILE_H */

