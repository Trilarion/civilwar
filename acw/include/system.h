/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef SYSTEM_H
#define SYSTEM_H

#ifndef __cplusplus
#error system.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	System Control
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.5  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.1  1994/03/10  14:29:08  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"
// #include "iconlib.h"
// #include "mouselib.h"

class Screen;
class SpriteLibrary;
class MouseLibrary;
class Image;
class Point;
class Rect;
class Sprite;
class Region;

/*
 * Class to hold together things for the main display screen
 */

class System {
public:
	Boolean initialised;		// True if everything has been initialised correctly

	MouseLibrary* mouse;
	Screen* screen;
	SpriteLibrary* mouseLibrary;
	SpriteLibrary* systemLibrary;

public:

	System()
	{
		initialised = False;
		screen = 0; mouse = 0; mouseLibrary = 0;
	}

	~System() { reset(); }

	void init();
	void reset();

public:

	/*
	 * Interface for main game
	 */
	
	void blit(Image* image, const Point& dest, const Rect& src);
	void blit(Image* image, const Point& dest);
	void unBlit(Image* image, const Point& where);
	void maskBlit(Sprite* image, const Point& dest);
};

extern System machine;

/*
 * Extra stuff that shouldn't really be here...
 */


#ifdef DEBUG
void drawPalette(Region* bm, const Point& where);
#endif
void showFullScreen(const char* name);

#if 0
void drawSprite(Region* bm, Point where, IconNumbers i);
void drawAnchoredSprite(Region* bm, Point where, IconNumbers i);
void drawCentredSprite(Region* bm, IconNumbers i);
#endif

void showString(const Point& p, const char* fmt, ...);


#ifdef DEBUG
void doBreak();
#endif

#endif /* SYSTEM_H */

