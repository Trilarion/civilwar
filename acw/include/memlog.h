/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef MEM_H
#define MEM_H

#ifndef __cplusplus
#error mem.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Memory Logging
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.3  1994/09/02  21:28:59  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/07/19  19:56:23  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/03/10  14:29:08  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

/*
 * resource Exhaustion management
 */

#include "types.h"

typedef Boolean (MemHandlerFunction)(size_t s);

struct MemHandler {
	MemHandlerFunction* function;
	MemHandler* next;
};

// MemHandler* setMemHandler(MemHandler* f);	// Function called when memory out

void addMemHandler(MemHandler* handle);
void removeMemHandler(MemHandler* handle);


#ifdef DEBUG

#include "log.h"

class MemBlock;


class MemoryLog {
	int lockCount;
	MemBlock* list;
	MemBlock* last;

	size_t total;

	void* newBlock;


	int newCount;
	int deleteCount;
	size_t maximum;

	MemBlock* makeMemBlock(size_t s);

	void lock() { lockCount++; }
	void unlock() { lockCount--; }
	int isLocked() { return lockCount; }

public:
	LogStream* logFile;
public:

	enum Mode { None, Full, Summary } mode;

	MemoryLog();
	~MemoryLog();

	void setMode(const char* name, Mode m);

	void* operator new(size_t s);
	void operator delete(void* p);

	void *allocate(size_t s);
	void removeBlock(void* p);

	void show();

	void track(const char* s);
	void addText(const char* s);
};

extern MemoryLog* memLog;


#endif		// DEBUG


#endif /* MEM_H */

