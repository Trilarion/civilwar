/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef ERROR_H
#define ERROR_H

#ifndef __cplusplus
#error error.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Exception Handling classes
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.2  1994/06/09  23:36:46  Steven_Green
 * Moved functions to seperate .cpp file so that it is easier to
 * add break points, and with a few to making it more sophisticated.
 *
 * Revision 1.1  1993/12/10  16:08:02  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"

class GeneralError {
	static Boolean doingError;
	static Boolean firstError;
	char *describe;
public:
	GeneralError();
	GeneralError(const char* s, ...);

	void setMessage(const char* s);
	const char* get();
};



#endif /* ERROR_H */

