/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef IMAGE_H
#define IMAGE_H

#ifndef __cplusplus
#error image.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * Bitmaps and Images
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.25  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.15  1994/01/07  23:45:53  Steven_Green
 * getWidth getHeight() added.
 * width/height fields replaced with size.
 *
 * Revision 1.14  1993/12/15  20:58:04  Steven_Green
 * HLine has overloaded versions for texturing.
 *
 * Revision 1.13  1993/12/10  16:08:02  Steven_Green
 * Gourad HLine added
 *
 * Revision 1.12  1993/12/04  16:26:00  Steven_Green
 * ImageError exception handling class added.
 *
 * Revision 1.11  1993/12/04  01:07:29  Steven_Green
 * Image::resize() added..
 *
 * Revision 1.10  1993/11/26  22:32:24  Steven_Green
 * virtual destructor added to image.
 *
 * Revision 1.9  1993/11/24  09:34:12  Steven_Green
 * Shape base object used for polygons instead of seperate routines for
 * each shape which BitMap had to know about.
 *
 * Revision 1.4  1993/11/05  16:52:08  Steven_Green
 * Mice, Sprites and Polygons added
 *
 * Revision 1.3  1993/10/27  21:09:15  Steven_Green
 * Clipping Rectangle added
 *
 * Revision 1.2  1993/10/26  21:33:33  Steven_Green
 * Horizontal Line, Vertical Line, Box and Frame functions added
 *
 * Revision 1.1  1993/10/26  14:54:37  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <string.h>

#ifndef GR_TYPES_H
#include "gr_types.h"
#endif

#ifndef ERROR_H
#include "error.h"
#endif

#include "palette.h"

class Sprite;
class TerrainCol;


/*
 * Store a 256 colour image
 */

class Image {
	Point size;
	UBYTE*		data;

public:
	UBYTE*	getAddress(SDimension x, SDimension y) const { return data + y * size.x + x; }
	UBYTE*	getAddress(Point p) const { return getAddress(p.x, p.y); }
	UBYTE*	getAddress() const { return data; }

				Image() : size(0,0) { data = 0; }
				Image(SDimension w, SDimension h);

		virtual	~Image();

		UBYTE* getAddress() { return data; }

		SDimension getWidth() const { return size.x; }
		SDimension getHeight() const { return size.y; }
		const Point& getSize() const { return size; }

		void	blit(const Image* image, Point dest, Rect src) const;
		void	maskBlit(const Sprite* sprite, Point dest, Rect src) const;

		Point	centre() { return Point(SDimension(size.x/2), SDimension(size.y/2)); }

		UBYTE* resize(SDimension w, SDimension h);
		UBYTE* resize(Point p) { return resize(p.x, p.y); }

		Image& operator = (const Image& src)
		{
			resize(src.size.x, src.size.y);
			memcpy(data, src.data, size.x * size.y);
			return *this;
		}

	/*
	 * For users that want to look after their own memory
	 */

	void setBuffer(UBYTE* buffer)
	{
		data = buffer;	 
	}

	UBYTE* getBuffer()
	{
		return data;
	}

	void setSize(Point p)
	{
		size = p;
	}	


	class ImageError : public GeneralError {
	public:
		ImageError(): GeneralError("Image Error") { }
		ImageError(char* s): GeneralError(s) { }
	};

	class NoMemory : public ImageError {
	public:
		NoMemory(): ImageError("Couldn't allocate memory for Image") { }
	};
};


class Region : public ClipRect {
protected:
	Image* image;			// Image that it refers to
public:
	Region();
	Region(Image* img, Rect area = Rect(0,0,0,0));

	Image*	getImage() { return image; }

	UBYTE*	getAddress(const Point& p)
	{
		return image->getAddress(p + *this);	// This is the cliprect's point
	}

	UBYTE*	getAddress(SDimension xx, SDimension yy)
	{
		return image->getAddress(xx + x, yy + y);	// This is the cliprect's point
	}

	UBYTE*	getAddress()
	{
		return image->getAddress(x, y);			// Address of start of window
	}


	SDimension getLineOffset() { return image->getWidth(); }

	void		fill(Colour col);
	void		fill(Image* pattern);
	void		plot(const Point& p, Colour col);
	void		HLine(SDimension x, SDimension y, SDimension length, Colour col);

	void		HLineFast(SDimension x, SDimension y, SDimension length, Colour col);
	void		HLineFast(SDimension x, SDimension y, SDimension length, Colour col, const Image* texture);								// Textured scan line

	void		HLineFast(SDimension x, SDimension y, SDimension length, TerrainCol* col, Intensity i1, Intensity i2);						// Gourad scan line
	void		HLineFast(SDimension x, SDimension y, SDimension length, TerrainCol* col, Intensity i1, Intensity i2, const Image* texture);	// Gourad Textured scan line

	void		VLine(SDimension x, SDimension y, SDimension length, Colour col);

	void		box(SDimension x, SDimension y, SDimension w, SDimension h, Colour col);
	void		box(const Rect& r, Colour c) { box(r.getX(), r.getY(), r.getW(), r.getH(), c); }

	void		greyOut(Rect r, Colour col);
	void		remap(Rect r, Colour* table);
	
	void		frame(SDimension x, SDimension y, SDimension w, SDimension h, Colour col);
	void		frame(SDimension x, SDimension y, SDimension w, SDimension h, Colour col1, Colour col2);

	void 		frame(const Rect& rect, Colour col1, Colour col2)
	{
		frame(rect.getX(), rect.getY(), rect.getW(), rect.getH(), col1, col2);
	}

	void 		frame(const Rect& rect, Colour col1)
	{
		frame(rect.getX(), rect.getY(), rect.getW(), rect.getH(), col1);
	}

	void		frame(const Point& corner, const Point& size, Colour col)
	{
		frame(corner.x, corner.y, size.x, size.y, col);
	}

	void		frame(const Point& corner, const Point& size, Colour col1, Colour col2)
	{
		frame(corner.x, corner.y, size.x, size.y, col1, col2);
	}

	void		line(const Point& from, const Point& to, Colour col);
	
	void 		setClip(SDimension x1 = 0, SDimension y1 = 0, SDimension w1 = 0, SDimension h1 = 0);

	void		blit(const Image* img, Point dest, Rect src) const
	{
		image->blit(img, dest + *this, src);
	}

	void		blit(const Image* img, Point pt)
	{
		image->blit(img, pt + *this, Rect(0, 0, img->getWidth(), img->getHeight()));
	}

	void		maskBlit(const Sprite* sprite, const Point& pt);

	void setBuffer(UBYTE* buffer) { image->setBuffer(buffer); }
	UBYTE* getBuffer() { return image->getBuffer(); }
	void setSize(Point p) { image->setSize(p); }

private:
	Boolean	inside(const Point& p);
	Boolean	clipLine(Point& p1, Point& p2);
};

inline void Region::HLineFast(SDimension x, SDimension y, SDimension length, Colour col)
{
	memset(getAddress(x, y), col, length);
}


/*
 * A region with its own image
 */

class Bitmap : public Region {
public:
	Bitmap();
	Bitmap(Point size);
	~Bitmap();

	void resize(Point size);
};

#endif /* IMAGE_H */
