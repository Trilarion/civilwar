/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *====================================================
 *
 * Sprite Definitions for game.lst
 *
 *====================================================
 */

#define SPR_icon_1 0
#define SPR_icon_2 19
#define SPR_icon_3 48
#define SPR_cityinfo 75
#define SPR_armyinfo 120
#define SPR_calicon 130
#define SPR_cpsm1rc2 152
#define SPR_flags 171
#define SPR_clock 191
#define SPR_submenu 196
#define SPR_fill 197
/* Total number of sprites = 205 */
