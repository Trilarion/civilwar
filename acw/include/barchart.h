/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef BARCHART_H
#define BARCHART_H

#ifndef __cplusplus
#error barchart.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Barchart drawing
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:12  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "image.h"

class UnitInfo;

void showStrengthBar(Region* bm, const UnitInfo* info, Rect r);
void showBar(Region* bm, int val, int maxVal, Rect r, const char* text);

#if 0
void showBarTransparent(Region* bm, int val, int maxVal, Rect r, const char* text);
#endif

#endif /* BARCHART_H */

