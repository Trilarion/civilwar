/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef GAMEICON_H
#define GAMEICON_H

#ifndef __cplusplus
#error gameicon.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Icons used within game
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */



#include "layout.h"
#include "menuicon.h"

/*=========================================================
 * General Purpose Icons
 */



class DosIcon : public MenuIcon {
public:
	DosIcon(IconSet* set, Point p, Key k1 = 0, Key k2 = 0) : MenuIcon(set, MM_Dos, Rect(p, Point(MAIN_ICON_W,MAIN_ICON_H)), k1, k2) { }
	void execute(Event* event, MenuData* d);
};

#if !defined(CAMPEDIT) && !defined(BATEDIT)

class SaveIcon : public MenuIcon {
public:
	SaveIcon(IconSet* set, Point p, Key k1 = 0, Key k2 = 0) : MenuIcon(set, MM_Save, Rect(p, Point(MAIN_ICON_W,MAIN_ICON_H)), k1, k2) { }
	void execute(Event* event, MenuData* d);

	// virtual void saveGame(const char* name) = 0;
};

class MainMenuIcon : public MenuIcon {
public:
	MainMenuIcon(IconSet* set, Point p, Key k1 = 0, Key k2 = 0);
	void execute(Event* event, MenuData* d);
};

class SoundMusicIcon : public MenuIcon {
public:
	SoundMusicIcon(IconSet* set, GameSprites n, Rect r) : MenuIcon(set, n, r) { }
	void execute(Event* event, MenuData* d);
};

class SoundIcon : public SoundMusicIcon {
public:
	SoundIcon(IconSet* set, Point p) : SoundMusicIcon(set, MM_Music, Rect(p, Point(MAIN_ICON_W,MAIN_ICON_H))) { }
};

class MiniSoundIcon : public SoundMusicIcon {
public:
	MiniSoundIcon(IconSet* set, Point p) :
		SoundMusicIcon(set, MM_MiniMusic, Rect(p, Point(MAIN_ICON_W,54))) { }
};

class OptionsIcon : public MenuIcon {
public:
	OptionsIcon(IconSet* set, Point p) :
		MenuIcon(set, MM_MiniOptions, Rect(p, Point(MAIN_ICON_W,53))) { }
};

#endif	// CAMPEDIT && BATEDIT

/*====================================================================
 * OK
 */

class MiniOkIcon : public MenuIcon {
public:
	MiniOkIcon(IconSet* parent, Point p, Key k1 = 0, Key k2 = 0) : MenuIcon(parent, MM_MiniOK, Rect(p, Point(120,48)), k1, k2) { }

	void execute(Event* event, MenuData* data);
};

class BigOkIcon : public MenuIcon {
public:
	BigOkIcon(IconSet* parent, Point p, Key k1 = 0, Key k2 = 0) :
		MenuIcon(parent, MM_BigOK, Rect(p, Point(123,107)), k1, k2) { }

	void execute(Event* event, MenuData* data);
};


/*
 * Quit (Keyboard only)
 */

class QuitIcon : public NullIcon {
public:
	QuitIcon(IconSet* parent) : NullIcon(parent, KEY_Escape) { }

	void execute(Event* event, MenuData* data);
};

#if !defined(CAMPEDIT) && !defined(BATEDIT)

class DatabaseIcon : public MenuIcon {
public:
	DatabaseIcon(IconSet* parent, Point p, Key k1 = 0, Key k2 = 0) :
		MenuIcon(parent, MM_Database, Rect(p, Point(MAIN_ICON_W,MAIN_ICON_H)), k1, k2) { }
	void execute(Event* event, MenuData* d);
	void drawIcon();
};

#endif

/*
 * Exceptions Used to get back to main menu or quit game
 */

class GotoMainMenu {
};

class GotoQuitGame {
};

class QuitDatabase {
};

#endif /* GAMEICON_H */

