/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef REGBAT_H
#define REGBAT_H

#ifndef __cplusplus
#error regbat.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Battle Data for Regiments
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/04/06  12:42:17  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

class Regiment;

class RegimentBattle {
public:
	/*
	 * Miscellaneous Info
	 */

	Regiment* regiment;			// What regiment does this data belong to

	/*
	 * Display Info
	 */

	/*
	 * AI Info
	 */

	/*
	 * Combat Info
	 */

public:
	RegimentBattle(Regiment* r);
	~RegimentBattle();
};

#endif /* REGBAT_H */

