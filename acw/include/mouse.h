/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef MOUSE_H
#define MOUSE_H

#ifndef __cplusplus
#error mouse.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * Mouse Driver Display and Interface
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.17  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.15  1994/04/22  22:16:24  Steven_Green
 * Icon Handling Tidied Up
 *
 * Revision 1.8  1994/01/07  23:45:53  Steven_Green
 * Mouse Events added
 *
 * Revision 1.7  1994/01/06  22:40:53  Steven_Green
 * ButtonStatus enumeration defined.
 *
 * Revision 1.6  1993/12/23  09:29:10  Steven_Green
 * init function added.
 *
 * Revision 1.5  1993/12/04  16:26:00  Steven_Green
 * Blit prototypes simplified by using default arguments
 *
 * Revision 1.2  1993/11/09  14:02:26  Steven_Green
 * MouseHandler typedef modified
 *
 * Revision 1.1  1993/11/05  16:52:08  Steven_Green
 * Initial revision
 *
 *----------------------------------------------------------------------
 */

#include "gr_types.h"
// #include "mouse_s.h"
#include "ptrlist.h"

class Sprite;
class Screen;
class Image;
class MouseEvent;
class MouseEventQueue;

typedef UBYTE Lock;

typedef short ButtonStatus;

struct MouseEvent {
	ButtonStatus buttons;
	Point where;
};


class MouseEventQueue {
	MouseEvent* events;
	int howMany;
	int head;
	int tail;
public:
	MouseEventQueue();
	~MouseEventQueue();

	Boolean isEmpty() const;
	MouseEvent* add(ButtonStatus b, Point p);
	MouseEvent* get();

	void clear();
};

class Mouse {
protected:
	Sprite*			sprite;			// What it looks like
	Screen*			screen;			// Where it is being displayed
	Image*			behind;			// What's behind the mouse?
	Point	position; 		// Where it currently is
	Point spritePosition;	// Top left of sprite
	Point				hotSpot;			// where the click point is relative to top left
	Lock hidden;  						// 0=displayed, >0 = Hidden
	Lock disabled;						// 0=enabled,   >0 = disabled
	Boolean initialised;					// 0=not initialised, 1=initialised
	MouseEventQueue events;

	volatile ButtonStatus lastButton;
public:

	enum MouseButton {
		LeftButton  = 0x0001,
		RightButton = 0x0002
	};


	// Values from mouse interrupt for testing use

	// volatile short event;
	volatile ButtonStatus buttons;
	volatile SDimension msX;
	volatile SDimension msY;

				Mouse(Screen* screen, Sprite* sprite, Point hotSpot) { init(screen, sprite, hotSpot); }
				Mouse() { }

			  ~Mouse();

	void		init(Screen* screen, Sprite* sprite, Point hotSpot);

	void		set(Point p);
	void		move(Point p) { hide(); set(p); show(); }
	void		moveMouse(Point p);
	void		hide();
	void		show();
	void		forceShow();

	void		disable() { disabled++; }
	void		enable();

	void		moveTo(Point pt);

	Boolean		isDisabled() { return disabled; }
	Boolean		isHidden() { return hidden; }

	void 		blitWithMouse(Image* image, const Point& dest, const Rect& src);

	Point		getPosition() { return position; }

	MouseEvent*	getEvent();
	MouseEvent* waitEvent();
	void clearEvents();

#ifdef MOUSE_CALLBACK
	friend void _mouseHandler(int event, int buttons, int xMove, int yMove);
#else	// MOUSE_UNDER_TIMER
	friend void _mouseHandler();
#endif
};

#ifdef DEBUG
extern Mouse* globalMouse;
#endif

#endif /* MOUSE_H */

