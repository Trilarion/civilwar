/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef ORDERS_H
#define ORDERS_H

#ifndef __cplusplus
#error orders.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Orders for Units
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.18  1994/09/23  13:29:31  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/02/28  23:05:43  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "gamedef.h"
#include "gametime.h"
#include "measure.h"

enum OrderMode {
	ORDER_Advance,
	ORDER_CautiousAdvance,
	ORDER_Attack,

	ORDER_Stand,
	ORDER_Hold,
	ORDER_Defend,

	ORDER_Withdraw,
	ORDER_Retreat,
	ORDER_HastyRetreat,

	ORDER_Max
};

enum BasicOrder {
	ORDERMODE_Advance,
	ORDERMODE_Hold,
	ORDERMODE_Withdraw
};

/*
 * IMPORTANT: Update table in campunit if this enueration is changed.
 */

enum OrderHow {
	ORDER_Land,
	ORDER_Rail,
	ORDER_Water,
	ORDER_Any,			// Move by fastest means

	ORDER_MaxHow
};

class Order {
public:
	OrderMode mode;			// Current orders
	OrderHow how;				// How it gets there
	Location destination;	// Location associated with order
	FacilityID onFacility;	// Rail/boat start point
	FacilityID offFacility;	// Rail boat disembark point
public:
	Order();
	Order(OrderMode o, OrderHow h, const Location& l);

	// void setOrder(const Order* o) { *this = *o; }
	void setOrder(OrderMode m, const Location& l)
	{
		mode = m;
		how = ORDER_Land;
		destination = l;
	}

	UWORD getOrderIcon() const;	// Really returns GameSprites
	BasicOrder basicOrder() const;
};

class SentOrder : public Order
{
public:
	TimeBase time;			// Time when order will reach destination
public:
	SentOrder() : Order() { }
	SentOrder(OrderMode mode, OrderHow how, const Location& l, const TimeBase& when);
	SentOrder(const Order& order, const TimeBase& when) : Order(order)
	{
		time = when;
	}
};

class BattleOrder {
public:
	OrderMode mode;			// Current orders
	Location destination;	// Location associated with order
public:
	BattleOrder();
	BattleOrder(OrderMode o, const Location& l);

	void setOrder(const BattleOrder* o) { *this = *o; }

	UWORD getOrderIcon() const;	// Really returns GameSprites
	BasicOrder basicOrder() const;

	int operator != (const BattleOrder& order) const
	{
		return (mode != order.mode) || (destination != order.destination);
	}

	int operator == (const BattleOrder& order) const
	{
		return (mode == order.mode) && (destination == order.destination);
	}
};

class SentBattleOrder : public BattleOrder
{
public:
	TimeBase time;			// Time when order will reach destination
public:
	SentBattleOrder() : BattleOrder() { }
	SentBattleOrder(OrderMode mode, const Location& l, const TimeBase& when);
};

BasicOrder getBasicOrder(OrderMode mode);


#endif /* ORDERS_H */
