/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Dialogue Box tester
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.7  1994/07/11  14:26:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.5  1994/05/25  23:31:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/05/19  17:44:37  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/05/04  22:09:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/04/26  13:39:07  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/04/22  22:14:22  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>
#include "dialogue.h"
#include "filesel.h"
#include "system.h"
#include "screen.h"
#include "menudata.h"

char input1[16] = "Input 1";
char input2[16] = "Input 2";

DialItem dialItems[] = {
	DialItem(Dial_FreeString, 1, Rect(0,0,120,12),   "Free String"),
	DialItem(Dial_Button,     2, Rect(0,100,80,16),  "Button1"),
	DialItem(Dial_Button,     3, Rect(88,100,80,16), "Button2"),
	DialItem(Dial_TextInput,  4, Rect(0,20,120,16),  input1, sizeof(input1)),
	DialItem(Dial_TextInput,  5, Rect(0,40,120,16),  input2, sizeof(input2)),
	DialItem(Dial_End)
};

class TestDial : public Dialogue {
public:
	TestDial() : Dialogue(dialItems) { }

	void doIcon(DialItem* item, Event* event, MenuData* d);
};

void TestDial::doIcon(DialItem* item, Event* event, MenuData* d)
{
	if(event->buttons)
	{
		switch(item->id)
		{
		case 2:
			d->setFinish(MenuData::MenuQuit);
			break;

		case 3:
			dialItems[0].text = "String Changed";
			d->setRedraw();
			break;
		}
	}
}

void main()
{
	try
	{
		machine.init(0, MemoryLog::None);
		showFullScreen("art\\screens\\playscrn.lbm");
 		machine.screen->update();

		{
			char path[FILENAME_MAX];

			strcpy(path, "gamedata\\*.dat");
			if(fileSelect(path, "Load file", True) == 0)
			{
				char buffer[500];

				sprintf(buffer, "Loading %s", path);

				dialAlert(0, buffer, "OK");
			}
			else
				dialAlert(0, "Load Cancelled", "OK");

		}

		{
			TestDial testDial;

			testDial.process();
		}


#if 0	  
		{
			MenuChoice choices[] = {
				MenuChoice("This", 1),
				MenuChoice("is a test", 2),
				MenuChoice("for", 0),
				MenuChoice("Menu Select", 4),
				MenuChoice(0,0)
			};

			menuSelect(0, choices);
		}

		{
			PopupText popup("Press any mouse button\rto clear this message");
			machine.screen->update();
			machine.mouse->waitEvent();
			popup.showText("And again!");
			machine.screen->update();
			// cleared automatically in clearup
		}

		dialAlert(0, "This is a message", "OK|Cancel");

		const int bufferSize = 16;
		char input[bufferSize+1];
		strcpy(input, "Default");
	
		dialInput(0, "Type some text\rinto the box", "Done|Ignore|Quit", input, bufferSize);
#endif
	}
 	catch(GeneralError e)
 	{
		cout << "Untrapped Error: ";
		if(e.get())
			cout << e.get();
		else
			cout << "No description";
		cout << endl;
 	}
 	catch(...)
 	{
 		cout << "Caught some kind of error" << endl;
 	}
}

/*
 * Functions undefined
 */

void checkScreenDump(MenuData* proc) { }
extern "C" {
void installPrtScn() { }
void removePrtScn() { }
}
