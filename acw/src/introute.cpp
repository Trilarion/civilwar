/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * Copyright (C) 1995, Steven Morle-Green, All rights Reserved
 *
 *----------------------------------------------------------------------
 *
 *	Calculate Best route between 2 points using land, rail or water
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "campwld.h"

GameTicks calcTimeToMove(Distance d, Speed s)
{
	GameTicks v1 = (d / s) * 256;
	GameTicks v2 = ((d % s) * 256) / s;

	return v1 + v2;
}

/*
 * Calculate best route between locations
 *
 * Fills in values in order (how, onFacility, offFacility)
 */

void CampaignWorld::getBestRoute(const Location& from, const Location& to, Order& order, Side side)
{
#ifdef DEBUG
	cLog.printf("---");
	cLog.printf("Calculating best route between %ld,%ld and %ld,%ld",
		from.x, from.y,
		to.x, to.y);
#endif

	const Speed walkSpeed = MilesPerHour(3);

	Distance mainDistance = distanceLocation(from, to);

	/*
	 * Try land
	 */

	GameTicks bestTime = calcTimeToMove(mainDistance, walkSpeed);
	OrderHow bestHow = ORDER_Land;
	FacilityID bestOn = NoFacility;
	FacilityID bestOff = NoFacility;

#ifdef DEBUG
	cLog.printf("Land time = %lu", bestTime);
#endif

	/*
	 * Try water route
	 */

	/*
	 * Find nearest landing stages
	 */

	FacilityID fid1 = findCloseLandingStage(from, side);
	FacilityID fid2 = findCloseWaterFacility(to);

	if((fid1 != NoFacility) && (fid2 != NoFacility) && (fid1 != fid2))
	{
		Facility* f1 = facilities[fid1];
		Facility* f2 = facilities[fid2];

		WaterZoneID zid1 = f1->waterZone;
		WaterZoneID zid2 = f2->waterZone;

		ASSERT(zid1 != NoWaterZone);
		ASSERT(zid2 != NoWaterZone);

		Distance endDistance = distanceLocation(f1->location, from) + 
								     distanceLocation(f2->location, to);
									  
		if(endDistance < mainDistance)
		{
			WaterRoute waterRoute;

			WaterZoneID wRoute = waterNet.findRoute(zid1, zid2, False, 0, &waterRoute);
			if(wRoute == NoWaterZone)
				wRoute = waterNet.findRoute(zid1, zid2, True, 0, &waterRoute);

			if(wRoute != NoWaterZone)
			{
				GameTicks wTime = waterRoute.route[zid2].cost;

				// Add on cost of getting to landing stages

				wTime += calcTimeToMove(endDistance, walkSpeed);

				wTime += GameTicksPerHour;	// Add on faffing about time

				if(wTime < bestTime)
				{
					bestTime = wTime;
					bestHow = ORDER_Water;
					bestOn = fid1;
					bestOff = fid2;
				}
#ifdef DEBUG
				cLog.printf("Water time = %lu (%s to %s)", 
					wTime,
					f1->getNameNotNull(),
					f2->getNameNotNull());
#endif
			}
		}
	}

	/*
	 * Try Rail route
	 */

	fid1 = findCloseRailhead(from, side);
	fid2 = findCloseRailJunction(to);

	if((fid1 != NoFacility) && (fid2 != NoFacility) && (fid1 != fid2))
	{
		Facility* f1 = facilities[fid1];
		Facility* f2 = facilities[fid2];
		
		Distance endDistance = distanceLocation(f1->location, from) + 
								     distanceLocation(f2->location, to);
									  
		if(endDistance < mainDistance)
		{
			RailRouteInfo info;

			FacilityID rRoute = findRailRoute(fid1, fid2, side, &info);
			if(rRoute == NoFacility)
				rRoute = findRailRoute(fid1, fid2, SIDE_None, &info);

			if(rRoute != NoFacility)
			{
				GameTicks wTime = info.route[fid2].cost;
				wTime += calcTimeToMove(endDistance, walkSpeed);
				wTime += GameTicksPerHour;	// Add on faffing about time

				if(wTime < bestTime)
				{
					bestTime = wTime;
					bestHow = ORDER_Rail;
					bestOn = fid1;
					bestOff = fid2;
				}
#ifdef DEBUG
				cLog.printf("Rail time = %lu (%s to %s)", 
					wTime,
					f1->getNameNotNull(),
					f2->getNameNotNull());
#endif
			}
		}
	}


	order.how = bestHow;
	order.onFacility = bestOn;
	order.offFacility = bestOff;
	order.destination = to;

#ifdef DEBUG
	cLog.printf("---");
	cLog.close();
#endif
}

