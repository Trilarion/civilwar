/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Game Menu Icon Implementation
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.25  1994/07/28  18:57:04  Steven_Green
 * menuicon drawIcon function uses drawCentredSprite()
 *
 * Revision 1.21  1994/06/09  23:32:59  Steven_Green
 * Changed Fonts
 *
 * Revision 1.19  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.1  1994/01/11  22:29:03  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "menuicon.h"
#include "system.h"
#include "screen.h"
#include "game.h"
#include "sprlib.h"
#include "colours.h"
#include "text.h"


#define Font_DummyIcon Font_EMFL15

/*
 * Basic Icon
 */

/*
 * Constructor
 */

MenuIcon::MenuIcon(IconSet* parent, GameSprites n, Rect r, Key k1, Key k2) :
		Icon(parent, r, k1, k2),
		iconNumber(n)
{
	// Do nothing
}

/*
 * Draw simple icon
 */

void MenuIcon::drawIcon()
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));
	game->sprites->drawCentredSprite(&bm, iconNumber);
	machine.screen->setUpdate(bm);
}

/*
 * Toggleable Icon
 */

ToggleMenuIcon::ToggleMenuIcon(IconSet* parent, GameSprites i1, GameSprites i2, Rect r, Boolean on, Key k1, Key k2) :
		MenuIcon(parent, on ? i2 : i1, r, k1, k2),
		icon1(i1),
		icon2(i2),
		state(on)
{
	// Do nothing
}

void ToggleMenuIcon::execute(Event* event, MenuData* data)
{
	data = data;

	if(event->buttons)
	{
		state = !state;

		setRedraw();
	}
}

void ToggleMenuIcon::drawIcon()
{
	if(state)
		setIcon(icon2);
	else
		setIcon(icon1);

	MenuIcon::drawIcon();
}



void DummyIcon::drawIcon()
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));
	bm.fill(LBlue);
	TextWindow window(&bm, Font_DummyIcon);
	window.setFormat(True, True);
	bm.frame(0, 0, getW(), getH(), Blue, LGrey);
	window.draw(text);
	machine.screen->setUpdate(bm);
}

