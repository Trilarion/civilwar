/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Mouse Library
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.5  1994/06/09  23:32:59  Steven_Green
 * Fixed crash when mouse pointer changed shape.
 *
 * Revision 1.4  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.3  1994/03/10  14:27:18  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/02/17  14:57:44  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/01/24  21:19:13  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "mouselib.h"


/*
 * Initialisation
 */

MouseLibrary::MouseLibrary(Screen* screen, SpriteLibrary* lib) :
	// SpriteLibrary("art\\mice"),
	Mouse()
{
	sprLib = lib;
	index = M_Busy;
	SpriteBlock* spriteImage = sprLib->read(index);
	Point hot = Point(spriteImage->anchorX, spriteImage->anchorY);

	init(screen, spriteImage, hot);
}

/*
 * Destruction
 */

MouseLibrary::~MouseLibrary()
{
	if(index != M_None)
	{
		sprLib->release(index);
		index = M_None;
	}
}

/*
 * Set Pointer style and return previous style
 */

PointerIndex MouseLibrary::setPointer(PointerIndex i)
{
	PointerIndex old = index;

	if(i != index)
	{
		PointerIndex oldIndex = index;

		index = i;
		SpriteBlock* spriteImage = sprLib->read(index);

		disable();
		hide();
		sprite = spriteImage;
		behind->resize(sprite->getWidth(), sprite->getHeight());
		hotSpot = Point(spriteImage->anchorX, spriteImage->anchorY);
		spritePosition = position - hotSpot;
		enable();
		show();

		if(oldIndex != M_None)
			sprLib->release(oldIndex);
	}

	return old;
}
