/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *      Civil War Introduction
 *
 *----------------------------------------------------------------------
 */

#include <stdlib.h>
#include <stdio.h>
#include "intro.h"
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_mixer.h"

  //-------------------------------------------------------------------------
  // Public Constructor
  //-------------------------------------------------------------------------
  Introduction::Introduction() {

    // Load the title graphics
    title = IMG_Load("../data/title.png");
    if ( title == NULL ) {
      fprintf(stderr, "Couldn't load title image: %s\n",SDL_GetError());
      exit(1);
    }
    
    // Initialize the Screen
    screen = SDL_SetVideoMode(800, 600, 16, SDL_SWSURFACE|SDL_FULLSCREEN);
    //screen = SDL_SetVideoMode(800, 800, 16, SDL_SWSURFACE | SDL_SRCALPHA | SDL_DOUBLEBUF);
    if ( screen == NULL ) {
      fprintf(stderr, "Unable to set 800x600 video: %s\n", SDL_GetError());
      exit(1);
    }

    wave = NULL;
  }

  //-------------------------------------------------------------------------
  // Public Destructor
  //-------------------------------------------------------------------------
  Introduction::~Introduction() {

    // Cean up the music
    if ( wave ) {
      Mix_FreeChunk(wave);
      wave = NULL;
    }
    Mix_CloseAudio();

    // Clean up the surfaces
    SDL_FreeSurface(title);
    SDL_FreeSurface(image);

  }

  //-------------------------------------------------------------------------
  // Run the introduction sequence
  //-------------------------------------------------------------------------
  void Introduction::run() {

    char filename[80];

    playAudio();

    titleDest.x = screen->w/2 - title->w/2;
    titleDest.y = 0;
    
    for (int i=1; i <= 20; i++) {
      sprintf(filename,"../data/acw%d.jpg",i);
      image = IMG_Load(filename);
      if ( image == NULL ) {
	fprintf(stderr, "Couldn't load sample.bmp: %s\n",SDL_GetError());
	exit(1);
      }

      fade (image, screen); 
      SDL_Delay(5500);
      SDL_FreeSurface(image);
    }
  }
    

  //-------------------------------------------------------------------------
  // Start playing the audio clip
  //-------------------------------------------------------------------------
  void Introduction::playAudio() {

    int audio_rate;
    Uint16 audio_format;
    int audio_channels;
    int loops = 0;

    // Start the audio playing
    audio_rate = MIX_DEFAULT_FREQUENCY;
    audio_format = MIX_DEFAULT_FORMAT;
    audio_channels = 2;
  
    if (Mix_OpenAudio(audio_rate, audio_format, audio_channels, 4096) < 0) {
      fprintf(stderr, "Couldn't open audio: %s\n", SDL_GetError());
      exit(2);
    }

    /* Load the requested wave file */
    wave = Mix_LoadWAV("../data/batlhymn.wav");
    if ( wave == NULL ) {
      fprintf(stderr, "Couldn't load audio file\n");
      exit(2);
    }

    /* Play and then exit */
    Mix_PlayChannel(0, wave, 0);
  }

  //-------------------------------------------------------------------------
  // Fade from one picture to the next
  //-------------------------------------------------------------------------
  void Introduction::fade(SDL_Surface *image, SDL_Surface *screen) {

    SDL_Rect dest, leftDest, rightDest;
    SDL_Surface *newImage;
    SDL_Surface *edge;
    SDL_Surface *leftEdge;
    SDL_Surface *rightEdge;

    dest.x = screen->w/2 - image->w/2;
    dest.y = screen->h/2 - image->h/2;
    dest.w = image->w;
    dest.h = image->h;

    leftDest.x = 0;
    leftDest.y = 0;
    leftDest.w = (screen->w - image->w)/2;
    leftDest.h = screen->h;

    rightDest.x = leftDest.w + image->w;
    rightDest.y = 0;
    rightDest.w = leftDest.w;
    rightDest.h = screen->h;

    edge = SDL_CreateRGBSurface(SDL_SWSURFACE | SDL_SRCALPHA,dest.x,screen->h,16,0x00,0x00,0x00, 0xFF);

    for(int alpha = 0; alpha < 256; alpha += 32) {
      SDL_SetAlpha(image, SDL_RLEACCEL | SDL_SRCALPHA, alpha);
      SDL_SetAlpha(edge, SDL_RLEACCEL | SDL_SRCALPHA, alpha);
      newImage = SDL_DisplayFormat(image);
      leftEdge = SDL_DisplayFormat(edge);
      rightEdge = SDL_DisplayFormat(edge);

      SDL_BlitSurface(leftEdge,NULL,screen, &leftDest);
      SDL_BlitSurface(image, NULL, screen, &dest);
      SDL_BlitSurface(rightEdge,NULL,screen, &rightDest);
      SDL_UpdateRect(screen, 0, 0, 0, 0 );

      SDL_BlitSurface(title,NULL,screen,&titleDest);
      SDL_UpdateRect(screen, titleDest.x, titleDest.y, title->w, title->h);

      SDL_FreeSurface(leftEdge);
      SDL_FreeSurface(newImage);
      SDL_FreeSurface(rightEdge);
    }
    SDL_FreeSurface(edge);
  }


//-------------------------------------------------------------------------
// Test the introduction sequence
//-------------------------------------------------------------------------
int main(int argc, char *argv[]) {

  Introduction *intro;
  SDL_Event event;

  if ( SDL_Init(SDL_INIT_AUDIO|SDL_INIT_VIDEO|SDL_INIT_TIMER) < 0 ) {
    fprintf(stderr, "Unable to init SDL: %s\n", SDL_GetError());
    exit(1);
  } 

  intro = new Introduction();
  intro->run();
  
  SDL_Quit();
  return 0;
}

/*
 *------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/04/01 13:34:14  greenius
 * main() has a return value.
 *
 *------------------------------------------------------
 */


    
