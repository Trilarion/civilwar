/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign Mode
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/07/04  13:26:52  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "campmode.h"
#include "campcity.h"
#include "campunit.h"
#include "mapwind.h"
#include "icon.h"
#include "menudata.h"

/*======================================================
 * Campaign Function functions
 */

CampaignFunction::CampaignFunction(CampaignMode* p)
{
	parent = p;
	oldPointer = p->getPointer();
}

CampaignFunction::~CampaignFunction()
{
	parent->setPointer(oldPointer);
}

void CampaignFunction::setPointer(PointerIndex p)
{
	parent->setPointer(p);
}

PointerIndex CampaignFunction::getPointer() const
{
	return parent->getPointer();
}

/*======================================================
 * Campaign Mode Functions
 */

CampaignMode::CampaignMode(MenuData* d)
{
	parent = d;
	zooming = False;
	function = 0;
	oldPointer = d->getPointer();
	d->setPointer(M_Arrow);
	setMode(CM_City);
}

CampaignMode::~CampaignMode()
{
	parent->setPointer(oldPointer);
	delete function;
}

void CampaignMode::setMode(CM_Mode newMode)
{
	if(function)
		delete function;

	mode = newMode;

	if(mode == CM_City)
		function = new TrackCity(this);
	else
		function = new TrackUnit(this);
}

void CampaignMode::toggleMode()
{
 	setMode( (mode == CM_City) ? CM_Troop : CM_City);
}

void CampaignMode::forceTroopMode(Unit* unit, Boolean giveOrders)
{
	setMode(CM_Troop);

	TrackUnit* track = (TrackUnit*) function;
	track->setCurrentUnit(unit, giveOrders);
}

void CampaignMode::overMap(MapWindow* map, Event* event, MenuData* d, const Location* l)
{
	overFlag = True;

	if(zooming)
	{
		if(event->buttons)
		{
			map->setStyle(MapWindow::Zoomed, *l);
			zooming = False;
			parent->setPointer(zoomPointer);
		}
	}
	else if(function)
		function->overMap(map, event, d, l);
}

void CampaignMode::startZoom()
{
	if ( !zooming )
	{
		zooming = True;
		zoomPointer = parent->getPointer();
		parent->setPointer(M_Zoom);
	}
	else
	{
	 	zooming = False;
		parent->setPointer( zoomPointer );
	}
}

GameSprites CampaignMode::getModeIcon()
{
	return (mode == CM_City) ? C_City : C_Troop;
}

void CampaignMode::setPointer(PointerIndex p)
{
	parent->setPointer(p);
}

PointerIndex CampaignMode::getPointer() const
{
	return parent->getPointer();
}

MenuData* CampaignFunction::getMenuData()
{
	return parent->getMenuData();
}

