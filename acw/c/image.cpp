/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Bitmap and Image Primitives
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.21  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.17  1994/01/07  23:43:18  Steven_Green
 * getWidth() and getHeight() used to access Image size.
 *
 * Revision 1.16  1993/12/15  20:56:36  Steven_Green
 * HLine moved out to seperate file (Hline.cpp)
 *
 * Revision 1.15  1993/12/13  21:59:58  Steven_Green
 * Gourad HLine speeded up using colour difference instead of intensity.
 *
 * Revision 1.14  1993/12/13  17:11:43  Steven_Green
 * Some modifications to Gourad HLine
 *
 * Revision 1.13  1993/12/13  14:09:20  Steven_Green
 * Gourad Scan Line debugged.  Uses Bresenham method.
 *
 * Revision 1.11  1993/12/04  16:22:51  Steven_Green
 * Exception handling added for lack of memory.
 *
 * Revision 1.10  1993/12/04  01:06:10  Steven_Green
 * Image::resize() added.
 *
 * Revision 1.5  1993/11/09  14:01:40  Steven_Green
 * Mouse Update Modification
 *
 * Revision 1.4  1993/11/05  16:50:53  Steven_Green
 * Support for sprites, mice and polygons added
 *
 * Revision 1.3  1993/10/27  21:08:17  Steven_Green
 * Bresenham Line drawing
 * IMage Clipping Rectangle
 *
 * Revision 1.2  1993/10/26  21:32:42  Steven_Green
 * Horizontal/Vertical Line routines added
 * Some clipping
 * Box and Frame
 *
 * Revision 1.1  1993/10/26  14:54:05  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifdef DEBUG
#include <iostream.h>
#endif
#include <string.h>

#include "image.h"
#include "vesa.h"
#include "sprite.h"
#include "miscfunc.h"


/*======================================================================
 * Raw Image functions
 */

Image::Image(SDimension width1, SDimension height1) : size(width1, height1)
{
	data = new UBYTE[width1 * height1];

	if(!data)
		throw NoMemory();
}

Image::~Image()
{
	if(data)
	{
		delete[] data;
		data = NULL;
	}
}

UBYTE* Image::resize(SDimension w, SDimension h)
{
	if((w != size.x) || (h != size.y))
	{
		if(data)
		{
			delete[] data;
		}

		if(w && h)
		{
			data = new UBYTE[w * h];

			if(!data)
				throw NoMemory();
		}
		else
			data = 0;

		size = Point(w, h);
	}

	return data;
}

/*
 * Mask Copy a line of image
 */

inline void maskCopy(Colour mask, UBYTE* dest, UBYTE* src, size_t length)
{
	while(length--)
	{
		Colour pixel = *src++;
		if(pixel != mask)
			*dest = pixel;
		dest++;
	}
}

/*
 * Copy part of an image onto another image
 */

void Image::blit(const Image* image, Point dest, Rect src) const
{
	if(src.getW() == 0)
		src = Rect(0,0,image->getWidth(), image->getHeight());

	ClipRect clip(0,0, image->getWidth(), image->getHeight());

	if(clip.clipSrc(dest, src))
		return;


	/*
	 * Clip to the destination image
	 */

	clip = ClipRect(0, 0, getWidth(), getHeight());

	if(clip.clip(dest, src))
		return;

	/*
	 * Calculate addresses
	 */

	UBYTE* fromLinePtr = image->getAddress(src);
	UBYTE* destLinePtr = getAddress(dest);

	int lineCount = src.getH();

	while(lineCount--)
	{
		memcpy(destLinePtr, fromLinePtr, src.getW());

		fromLinePtr += image->getWidth();
		destLinePtr += getWidth();
	}

}

/*
 * Copy part of an image onto another image with masking
 */

void Image::maskBlit(const Sprite* sprite, Point dest, Rect src) const
{
	if(src.getW() == 0)
		src = Rect(0,0,sprite->getWidth(), sprite->getHeight());

	ClipRect clip(0, 0, getWidth(), getHeight());

	if(clip.clip(dest, src))
		return;

	/*
	 * Calculate addresses
	 */

	UBYTE* fromLinePtr = sprite->getAddress(src);
	UBYTE* destLinePtr = getAddress(dest);

	int lineCount = src.getH();

	while(lineCount--)
	{
		maskCopy(sprite->transparent, destLinePtr, fromLinePtr, src.getW());

		fromLinePtr += sprite->getWidth();
		destLinePtr += getWidth();
	}

}

/*===================================================================
 * Region Functions
 */

/*
 * Set up a region
 */

Region::Region(Image* img, Rect area)
{
	image = img;

	if(area.getW() == 0)
		setClip(0, 0, img->getWidth(), img->getHeight());
	else
		setClip(area.getX(), area.getY(), area.getW(), area.getH());
}

Region::Region()
{
	image = 0;
	setClip(0,0,0,0);
}

void Region::fill(Colour col)
{
	box(0, 0, getW(), getH(), col);
}

void Region::fill(Image* pattern)
{
	UBYTE* destPtr = getAddress();

	int srcX = getX() % pattern->getWidth();
	int srcY = getY() % pattern->getHeight();

	UBYTE* srcPtr = pattern->getAddress(srcX, srcY);

	int yCount = getH();
	while(yCount--)
	{
		UBYTE* dPtr = destPtr;
		UBYTE* sPtr = srcPtr;
		int sX = srcX;

		int xCount = getW();
		while(xCount--)
		{
			*dPtr++ = *sPtr++;
			sX++;
			if(sX >= pattern->getWidth())
			{
				sX = 0;
				sPtr -= pattern->getWidth();
			}
		}

		srcPtr += pattern->getWidth();
		destPtr += getLineOffset();

		srcY++;
		if(srcY >= pattern->getHeight())
		{
			srcY = 0;
			srcPtr = pattern->getAddress(srcX, srcY);
		}
	}
}

void Region::plot(const Point& p, Colour col)
{
	if(inside(p))
		*getAddress(p) = col;
}

void Region::VLine(SDimension x, SDimension y, SDimension length, Colour col)
{
	/*
	 * Make sure it goes from top to bottom
	 */

	if(length < 0)
	{
		y += length;
		length = SDimension(-length);
	}

	/*
	 * Is it on the bitmap?
	 */

	if( (x < 0) || (x >= getW()) )
		return;

	/*
	 * Adjust Y/length to fit bitmap
	 */

	if(y < 0)
	{
		length += y;		// y is -ve so this makes it shorter
		y = 0;
	}

	if( (y + length) >= getH())
	{
		length = (SDimension) (getH() - y);
	}

	if(length <= 0)
		return;

	UBYTE* ad = getAddress(Point(x, y));
	
	while(length--)
	{
		*ad = col;

		ad += image->getWidth();
	}
}

void Region::box(SDimension x, SDimension y, SDimension w, SDimension h, Colour col)
{
	/*
	 * Clip it
	 */

	if(x < 0)
	{
		w += x;
		x = 0;
	}

	if( (x + w) > getW())
	{
		w = (SDimension) (getW() - x);
	}

	if(w <= 0)
		return;

	if(y < 0)
	{
		h += y;
		y = 0;
	}

	if( (y + h) > getH())
	{
		h = (SDimension) (getH() - y);
	}

	if(h <= 0)
		return;
	
	
	
	UBYTE *ad = getAddress(Point(x, y));

	while(h--)
	{
		memset(ad, col, w);
		ad += image->getWidth();
	}
}


void Region::greyOut(Rect r, Colour col)
{
	SDimension x = r.x;
	SDimension y = r.y;
	SDimension w = r.getW();
	SDimension h = r.getH();

	/*
	 * Clip it
	 */

	if(x < 0)
	{
		w += x;
		x = 0;
	}

	if( (x + w) > getW())
	{
		w = (SDimension) (getW() - x);
	}

	if(w <= 0)
		return;

	if(y < 0)
	{
		h += y;
		y = 0;
	}

	if( (y + h) > getH())
	{
		h = (SDimension) (getH() - y);
	}

	if(h <= 0)
		return;
	
	
	
	UBYTE *ad = getAddress(Point(x, y));
	int mod = 0;

	while(h--)
	{
		UBYTE* ad1 = ad;

		SDimension w1 = w;

		int mod1 = mod;

		while(w1--)
		{

			if(mod1)
				*ad1 = col;

			ad1++;

			if(mod1 == 2)
				mod1 = 0;
			else
				mod1++;
		}

		if(mod == 2)
			mod = 0;
		else
			mod++;

		ad += image->getWidth();
	}
}

void Region::remap(Rect r, Colour* table)
{
	SDimension x = r.x;
	SDimension y = r.y;
	SDimension w = r.getW();
	SDimension h = r.getH();

	/*
	 * Clip it
	 */

	if(x < 0)
	{
		w += x;
		x = 0;
	}

	if( (x + w) > getW())
	{
		w = (SDimension) (getW() - x);
	}

	if(w <= 0)
		return;

	if(y < 0)
	{
		h += y;
		y = 0;
	}

	if( (y + h) > getH())
	{
		h = (SDimension) (getH() - y);
	}

	if(h <= 0)
		return;
	
	
	
	UBYTE *ad = getAddress(Point(x, y));
	int mod = 0;

	while(h--)
	{
		UBYTE* ad1 = ad;

		SDimension w1 = w;

		while(w1--)
		{
			*ad1 = table[*ad1];
			ad1++;
		}

		ad += image->getWidth();
	}
}



void Region::frame(SDimension x, SDimension y, SDimension w, SDimension h, Colour col)
{
	HLine(x,         y,         w, col);
	HLine(x,         SDimension(y + h - 1), w, col);
	VLine(x,         y,         h, col);
	VLine(SDimension(x + w - 1), y,         h, col);
}

void Region::frame(SDimension x, SDimension y, SDimension w, SDimension h, Colour col1, Colour col2)
{
	HLine(x,         y,         w, col1);
	HLine(x,         SDimension(y + h - 1), w, col2);
	VLine(x,         y,         h, col1);
	VLine(SDimension(x + w - 1), y,         h, col2);
}

/*
 * General case line drawing
 */

static SDimension clipEdge(SDimension edge, const Point& p1, const Point& p2)
{

	Point left;
	Point right;

	if(p1.x > p2.x)
	{
		left = p2;
		right = p1;
	}
	else
	{
		left = p1;
		right = p2;
	}

	while( (left.x != edge) && (left.y != right.y))
	{
		// Point mid = left + ((right - left + Point(1,1)) >> 1);
		Point mid;

		mid.x = left.x + ((right.x - left.x + 1) >> 1);
		mid.y = left.y + ((right.y - left.y + 1) >> 1);

		if(mid.x <= edge)
			left = mid;
		else
			right = mid;

	}

	return left.y;
}

/*
 * Clip a line to a region's boundary
 * return True if anything left on screen
 *        False if nothing left
 *
 * Copy CohenSutherland (see Foley/VanDam/etc pg. 116)
 *
 * Note that points are relative to window, so are clipped
 * to (0,0,w,h) rather than (x,y,maxX,maxY)
 */

#define CLIP_TOP		1
#define CLIP_BOTTOM	2
#define CLIP_LEFT		4
#define CLIP_RIGHT	8

inline UBYTE getOutCode(const Point& p, const ClipRect& rect)
{
	UBYTE value = 0;

	if(p.y >= rect.getH())
		value = CLIP_TOP;
	else if(p.y < 0)
		value = CLIP_BOTTOM;

	if(p.x >= rect.getW())
		value |= CLIP_RIGHT;
	else if(p.x < 0)
		value |= CLIP_LEFT;

	return value;
}

Boolean Region::clipLine(Point& p1, Point& p2)
{
	for(;;)
	{
		UBYTE outcode0 = getOutCode(p1, *this);
		UBYTE outcode1 = getOutCode(p2, *this);

		/*
		 * Both points on screen if outcodes are both 0
		 */

		if( (outcode0 | outcode1) == 0)
			return True;

		/*
		 * Points in same section, so it is all clipped!
		 */

		if( (outcode0 & outcode1) != 0)
			return False;

		/*
		 * Find one of the points outside the rectangle
		 */

		UBYTE outCode;
		Point* p;
		
		if(outcode0)
		{
			outCode = outcode0;
			p = &p1;
		}
		else
		{
			outCode = outcode1;
			p = &p2;
		}

		if(outCode & CLIP_TOP)
		{
			p->x = clipEdge(getH() - 1, Point(p1.y, p1.x), Point(p2.y, p2.x));
			p->y = getH() - 1;
		}
		else if(outCode & CLIP_BOTTOM)
		{
			p->x = clipEdge(0, Point(p1.y, p1.x), Point(p2.y, p2.x));
			p->y = 0;
		}
		else if(outCode & CLIP_RIGHT)
		{
			p->y = clipEdge(getW() - 1, p1, p2);
			p->x = getW() - 1;
		}
		else if(outCode & CLIP_LEFT)
		{
			p->y = clipEdge(0, p1, p2);
			p->x = 0;
		}
	}	// Loop again with clipped segment
}


void Region::line(const Point& fromPt, const Point& toPt, Colour col)
{
	Point p1 = fromPt;
	Point p2 = toPt;

	if(!clipLine(p1, p2))
		return;

	/*
	 * Is it a point?
	 */

	if(p1 == p2)
	{
		*getAddress(p1) = col;
		return;
	}

	/*
	 * Is it a Vertical Line?
	 */

	if(p1.x == p2.x)
	{
		SDimension length;
		if(p1.y > p2.y)
			swap(p1.y, p2.y);
		length = p2.y - p1.y + 1;
		VLine(p1.x, p1.y, length, col);
		return;
	}

	/*
	 * Is it a horizontal line?
	 */

	else if(p1.y == p2.y)
	{
		SDimension length;
		if(p1.x > p2.x)
			swap(p1.x, p2.x);
		length = p2.x - p1.x + 1;
		HLine(p1.x, p1.y, length, col);
		return;
	}

#if 0
	/*
	 * It is a general purpose line
	 */

	Point p1 = p1;
	Point p2 = p2;

	Point* left;
	Point* right;
	Point* p2p;
	Point* bottom;

	Point* & oleft = left;
	Point* & oright = right;
	Point* & otop = top;
	Point* & obottom = bottom;

	if(p1.x < p2.x)
	{
		left = &p1;
		right = &p2;
	}
	else
	{
		left = &p2;
		right = &p1;
	}

	if(p1.y < p2.y)
	{
		top = &p1;
		bottom = &p2;
	}
	else
	{
		top = &p2;
		bottom = &p1;
	}

	/*
	 * Is any of line on screen?
	 */

	if(right->x < 0)
		return;
	if(left->x >= getW())
		return;
	if(bottom->y < 0)
		return;
	if(top->y >= getH())
		return;

	/*
	 * Left Clip
	 */

	if(left->x < 0)
	{
		left->y = clipEdge(0, oleft, oright);
		left->x = 0;
	}
	
	/*
	 * Right Clip
	 */

	if(left->x >= getW())
		return;
	if(right->x >= getW())
	{
		right->y = clipEdge(SDimension(getW() - 1), oleft, oright);
		right->x = (SDimension) (getW() - 1);
	}

	/*
	 * Top Clip
	 */

	if(bottom->y < 0)
		return;
	if(top->y < 0)
	{
		Point p1 = Point(otop->y, otop->x);
		Point p2 = Point(obottom->y, obottom->x);

		top->x = clipEdge(0, &p1, &p2);
		top->y = 0;
	}

	/*
	 * Bottom Clip
	 */

	if(top->y >= getH())
		return;
	if(bottom->y >= getH())
	{
		Point p1 = Point(otop->y, otop->x);
		Point p2 = Point(obottom->y, obottom->x);

		bottom->x = clipEdge(SDimension(getH() - 1), &p1, &p2);
		bottom->y = (SDimension) (getH() - 1);
	}
#endif

	/*
	 * Work out Bresenham terms
	 */

	int dx = p2.x - p1.x;
	int dy = p2.y - p1.y;

	unsigned int adx = (dx < 0) ? -dx : +dx;
	unsigned int ady = (dy < 0) ? -dy : +dy;

	/*
	 * Which is the major axis?
	 */

	if(adx > ady)
	{
		// X is the main axis

		// Make sure we are going from left to right

		if(dx < 0)
		{
			// swap(p1, p2);
			swap(p1.x, p2.x);
			swap(p1.y, p2.y);
			dy = -dy;
		}

		UBYTE* ad = getAddress(p1);

		*ad = col;

		if(p1.x != p2.x)
		{
			int value = 0x8000;								// Initial Bresenham term
			int add = (ady * 0x10000) / adx;				// Amount to add
			int ddy = (dy < 0) ? -image->getWidth() : +image->getWidth();		// Line offset

			while(p1.x++ < p2.x)
			{
				value += add;
				if(value >= 0x10000)
				{
					ad += ddy;
					value -= 0x10000;
				}
				ad++;

				*ad = col;
			}
		}
	}
	else
	{
		// Y is the main axis

		// Make sure we are going from Top to Bottom

		if(dy < 0)
		{
			Point tPoint = p1;
			p1 = p2;
			p2 = tPoint;

			dx = -dx;
		}

		UBYTE* ad = getAddress(p1);

		*ad = col;

		if(p1.y != p2.y)
		{
			int value = 0x8000;								// Initial Bresenham term
			int add = (adx * 0x10000) / ady;				// Amount to add
			int ddx = (dx < 0) ? -1 : +1;					// Offset to move left/right

			while(p1.y++ < p2.y)
			{
				value += add;
				if(value >= 0x10000)
				{
					ad += ddx;
					value -= 0x10000;
				}
				ad += image->getWidth();

				*ad = col;
			}
		}

	}
}

void Region::setClip(SDimension x1, SDimension y1, SDimension w1, SDimension h1)
{
	if(w1 == 0)
		ClipRect::setClip(0, 0, image->getWidth(), image->getHeight());
	else
	{
		/*
		 * Make sure values are within image
		 */

		if(x1 < 0)
			x1 = 0;
		if(y1 < 0)
			y1 = 0;
		if(x1 >= image->getWidth())
			x1 = SDimension(image->getWidth() - 1);
		if(y1 >= image->getHeight())
			y1 = SDimension(image->getHeight() - 1);

		if( (x1 + w1) > image->getWidth())
			w1 = (SDimension)(image->getWidth() - x1);

		if( (y1 + h1) > image->getHeight())
			h1 = (SDimension)(image->getHeight() - y1);


		ClipRect::setClip(x1,y1,w1,h1);
	}
}

void Region::maskBlit(const Sprite* sprite, const Point& pt)
{
	Rect src = Rect(0,0,sprite->getWidth(), sprite->getHeight());
	Point dest = pt + *this;

	if(clip(dest, src))
		return;

	/*
	 * Calculate addresses
	 */

	UBYTE* fromLinePtr = sprite->getAddress(src);
	UBYTE* destLinePtr = image->getAddress(dest);

	int lineCount = src.getH();

	while(lineCount--)
	{
		maskCopy(sprite->transparent, destLinePtr, fromLinePtr, src.getW());

		fromLinePtr += sprite->getWidth();
		destLinePtr += image->getWidth();
	}

}

/*-----------------------------------------------------------------
 * Private support functions
 */

/*
 * Return True if point is inside clip rectangle
 */

Boolean Region::inside(const Point& p)
{
	return (p.x >= 0) && (p.x < getW()) &&
			 (p.y >= 0) && (p.y < getH());
}


/*=============================================================
 * Clipping functions
 */

/*
 * Rectangle Overlap operator
 * if they don't overlap then width and height are 0
 */

Rect Rect::operator &(const Rect r1) const
{
	Rect result(0,0,0,0);

	if( (this->x < (r1.x + r1.size.x)) &&
		 (r1.x < (this->x + this->size.x)) &&
		 (this->y < (r1.y + r1.size.y)) &&
		 (r1.y < (this->y + this->size.y)) )
	{
		const Rect* left;
		const Rect* right;

		if(x < r1.x)
		{
			left = this;
			right = &r1;
		}
		else
		{
			left = &r1;
			right = this;
		}
	  
		result.x = right->x;

		SDimension x1 = SDimension(left->x + left->size.x);
		SDimension x2 = SDimension(right->x + right->size.x);

		result.size.x = SDimension(((x1 < x2) ? x1 : x2) - right->x);

		const Rect* upper;
		const Rect* lower;

		if(y < r1.y)
		{
			upper = this;
			lower = &r1;
		}
		else
		{
			upper = &r1;
			lower = this;
		}

		result.y = lower->y;

		SDimension y1 = SDimension(upper->y + upper->size.y);
		SDimension y2 = SDimension(lower->y + lower->size.y);

		result.size.y = SDimension(((y1 < y2) ? y1 : y2) - lower->y);
	}

	return result;
}

/*
 * Return True if point is inside clip rectangle
 */

Boolean ClipRect::inside(const Point& p)
{
	return (p.x >= x) && (p.x < max.x) &&
			 (p.y >= y) && (p.y < max.y);
}

/*
 * Clip a source rectangle with a clipping rectangle
 *
 * Return 0 if something to display
 * non zero if all off screen
 */

int ClipRect::clip(Point& dest, Rect& src) const
{
	if(dest.x < x)
	{
		src.setW(SDimension(src.getW() - (x - dest.x)));
		src.x += x - dest.x;
		dest.x = x;

		if(src.getW() <= 0)
			return -1;
	}

	if(dest.y < y)
	{
		src.setH(SDimension(src.getH() - (y - dest.y)));
		src.y += y - dest.y;
		dest.y = y;

		if(src.getH() <= 0)
			return -1;
	}

	if( (dest.x + src.getW()) > (x + getW()))
	{
		src.setW(SDimension(src.getW() - ( (dest.x + src.getW()) - (x + getW()) )));

		if(src.getW() <= 0)
			return -1;
	}

	if( (dest.y + src.getH()) > (y + getH()) )
	{
		src.setH(SDimension(src.getH() - ((dest.y + src.getH()) - (y + getH()))));

		if(src.getH() <= 0)
			return -1;
	}

	return 0;
}

/*
 * Clip to src coordinates
 */

	
int ClipRect::clipSrc(Point& dest, Rect& src) const
{
	if(src.x < x)
	{
		src.setW(SDimension(src.getW() - (x - src.x)));
		dest.x += x - src.x;
		src.x = x;

		if(src.getW() <= 0)
			return -1;
	}

	if(src.y < y)
	{
		src.setH(SDimension(src.getH() - (y - src.y)));
		dest.y += y - src.y;
		src.y = y;

		if(src.getH() <= 0)
			return -1;
	}

	if( (src.x + src.getW()) > (x + getW()))
	{
		src.setW(SDimension(src.getW() - ( (src.x + src.getW()) - (x + getW()) )));

		if(src.getW() <= 0)
			return -1;
	}

	if( (src.y + src.getH()) > (y + getH()) )
	{
		src.setH(SDimension(src.getH() - ((src.y + src.getH()) - (y + getH()))));

		if(src.getH() <= 0)
			return -1;
	}

	return 0;
}


/*
 * Bitmaps are regions with their own image
 */

Bitmap::Bitmap(Point p)
{
	image = new Image(p.x, p.y);
	setClip(0,0,p.x,p.y);
}

Bitmap::Bitmap() : Region()
{
	image = new Image;
}

void Bitmap::resize(Point s)
{
	if(image)
		image->resize(s);
	else
	{
		image = new Image(s.x, s.y);
	}

	setClip(0, 0, s.x, s.y);
}

Bitmap::~Bitmap()
{
	delete image;
}
