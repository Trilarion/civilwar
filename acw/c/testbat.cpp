/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Battle Tester
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.2  1994/08/31  15:23:00  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/08/09  15:42:15  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#define TESTBAT_H

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <conio.h>
#include <ctype.h>

#include "batsel.h"
#include "game.h"
#include "sprlib.h"
#include "system.h"
#include "screen.h"
#include "ilbm.h"
#include "colours.h"
#include "text.h"
#include "generals.h"

#ifndef DEBUG
#include <iostream.h>
#endif

#ifdef DEBUG
#include "options.h"
#include "memlog.h"
#endif
#include "sound.h"
#include "layout.h"
#include "memptr.h"


#define Font_SimpleMessage Font_EMFL10
#define MsgFillColour 0x1d
#define MsgBorderColour1 0x1f
#define MsgBorderColour2 0x50

/*
 * Global Variables
 *
 * Merely defining this, automatically sets up the screen mode and mouse
 * pointer
 */

GameVariables* game = 0;

#ifdef DEBUG
Boolean disableAI = False;
#endif

class GameControl {
public:
	GameControl(GameType gt) { game = new GameVariables(gt); }
	~GameControl() { delete game; game = 0; }

#ifdef DEBUG		 // To optionally set AI side
	void setplayersSide( Side s ) { game->playersSide = s; }
#endif

};

/*
 * Set up global variables
 */


GameVariables::GameVariables(GameType gt)
{
	quit = False;

	gameType = gt;
	playersSide = SIDE_USA;
	playerCount = 1;
	connection = 0;
	sprites = new SpriteLibrary("art\\game");
}

GameVariables::~GameVariables()
{
	if(sprites)
		delete sprites;
}

/*
 * Quick bodge to display a simple text box
 */

void GameVariables::simpleMessage(const char* fmt, ...)
{
	Region bm(machine.screen->getImage(), Rect(MSGW_X, MSGW_Y, MSGW_W, MSGW_H));
	bm.fill(MsgFillColour);
	bm.frame(0,0,MSGW_W-1, MSGW_H-1, MsgBorderColour1, MsgBorderColour2);
	if(fmt)
	{
		MemPtr<char> buffer(1024);

		va_list vaList;
		va_start(vaList, fmt);
		vsprintf(buffer, fmt, vaList);
		va_end(vaList);

		TextWindow win(&bm, Font_SimpleMessage);
		win.setFormat(True, True);
		win.setColours(Black);
		win.draw(buffer);
	}
	machine.screen->setUpdate(bm);
}


/*
 * Handle global packets
 */

Packet* GameVariables::receiveData()
{
	return 0;
}

void GameVariables::disconnect()
{
}

/*
 * Some functions to keep the linker happy
 */

MapPriority Army::isVisible(MapWindow* map) const
{
	return Map_NotVisible;
}

void Army::mapDraw(MapWindow* map, Region* bm, Point where)
{
}

MapPriority Corps::isVisible(MapWindow* map) const
{
	return Map_NotVisible;
}

void Corps::mapDraw(MapWindow* map, Region* bm, Point where)
{
}

MapPriority Division::isVisible(MapWindow* map) const
{
	return Map_NotVisible;
}

void Division::mapDraw(MapWindow* map, Region* bm, Point where)
{
}

MapPriority Brigade::isVisible(MapWindow* map) const
{
	return Map_NotVisible;
}


void Brigade::mapDraw(MapWindow* map, Region* bm, Point where)
{
}

MapPriority Regiment::isVisible(MapWindow* map) const
{
	return Map_NotVisible;
}

void Regiment::mapDraw(MapWindow* map, Region* bm, Point where)
{
}


MapPriority Unit::isVisible(MapWindow* map) const
{
	return Map_NotVisible;
}

void Unit::mapDraw(MapWindow* map, Region* bm, Point where)
{
}

void Unit::drawCampaignUnit(MapWindow* map, Region* bm, Point& where, UWORD icon)
{
}

void Unit::showCampaignInfo(Region* window)
{
}

void Unit::beforeBattle()
{
}

void Unit::afterBattle()
{
}


/*
 * main function
 */

int main(int argc, char *argv[])
{
	try
	{
		/*
		 * Process command line
		 */

#ifdef DEBUG
		MemoryLog::Mode memMode = MemoryLog::None;
		int debugLevel = 0;
		AI_ForceOption = 0;
#endif

		int i = 0;
		while(++i < argc)
		{
			const char* arg = argv[i];

			if((arg[0] == '-') || (arg[0] == '/'))
			{
				switch(toupper(arg[1]))
				{
#ifdef DEBUG
				case 'M':										// -M for Memory Logging
					if(toupper(arg[2]) == 'S')				// -MS for memory summary
						memMode = MemoryLog::Summary;
					else
						memMode = MemoryLog::Full;
					break;

				case 'D':										// -D for debug log
					if(isdigit(arg[2]))
						debugLevel = atoi(&arg[2]);
					else
						debugLevel = 32767;
					break;

				case 'P':										// Used to select AI side 
					if ( toupper(arg[2]) == 'C')			
						AI_ForceOption = 80;
					else if ( toupper(arg[2]) == 'U')
						AI_ForceOption = 160;
					else break;

					if ( isdigit( arg[3] ) )
					{
						if ( isdigit( arg[4] ) )
						{
							AI_ForceOption += ( arg[3] - '0' ) * 10;
							AI_ForceOption += ( arg[4] - '0' );
						}
						else AI_ForceOption += ( arg[3] - '0' );
					}

					break;
				case 'A':
					disableAI = True;
					break;
#endif
				}
			}
		}


		machine.init();
#ifdef DEBUG
		memLog->setMode("memory.log", memMode);
		if(debugLevel)
			logFile = new LogStream("debug.log", debugLevel);
#endif
		sound.init();

		GameControl gameControl(BattleGame);

#ifdef DEBUG
		if ( AI_ForceOption >= 160 ) gameControl.setplayersSide( SIDE_CSA );
#endif
		
		// playBattle(0);
		chooseBattle();
	}
 	catch(GeneralError e)
 	{
		cout << "Untrapped Error: ";
		if(e.get())
			cout << e.get();
		else
			cout << "No description";
		cout << endl;

		while(!kbhit());
 	}
 	catch(...)
 	{
 		cout << "Caught some kind of error" << endl;

		while(!kbhit());
 	}

	return 0;
}


