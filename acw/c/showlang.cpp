/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Utility to show what strings are in a language file
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>
#include "types.h"

void main(int argc, char* argv[])
{
	if(argc != 2)
	{
		printf("Usage: showlang filename");
		return;
	}

	FILE* fp = fopen(argv[1], "rb");
	if(fp)
	{
		UWORD nStrings;

		fread(&nStrings, sizeof(nStrings), 1, fp);

		printf("There are %u strings\n", nStrings);

		for(int count = 0; count < nStrings; count++)
		{
			ULONG pos;

			printf("%4d : ", count);

			fseek(fp, count * 4 + 2, SEEK_SET);
			fread(&pos, sizeof(pos), 1, fp);
			fseek(fp, pos, SEEK_SET);

			UWORD length;
			fread(&length, sizeof(length), 1, fp);

			while(length--)
			{
				char c = fgetc(fp);

				if(c == '\r')
					printf("\n%4s : ", "");
				else
					putchar(c);
			}
			printf("\n");

		}


		fclose(fp);
	}
	else
		printf("Can't open %s\n", argv[1]);
}

