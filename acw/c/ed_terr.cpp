/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Terrain Editor
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "camped.h"
#include "campter.h"
#include "campwld.h"
#include "mapwind.h"
#include "ilbm.h"
#include "system.h"
#include "screen.h"
#include "mouselib.h"
#include "text.h"
#include "colours.h"

static char terrainFileName[] = "art\\terrmap.lbm";

static char* terrainStr[] = {
	"Unused0",
	"Sea",
	"Coastal",
	"River",
	"Stream",
	"Swamp",
	"Wooded",
	"Farmland",
	"Mountain",
	"Rough",
	"Impassable",
	"Unused11",
	"Unused12",
	"Unused13",
	"Unused14",
	"Unused15"
};

static MenuChoice terrainChoice[] = {
	MenuChoice(terrainStr[CT_Unused0],		1+CT_Unused0),
	MenuChoice(terrainStr[CT_Sea],			1+CT_Sea),
	MenuChoice(terrainStr[CT_Coastal],		1+CT_Coastal),
	MenuChoice(terrainStr[CT_River],			1+CT_River),
	MenuChoice(terrainStr[CT_Stream],		1+CT_Stream),
	MenuChoice(terrainStr[CT_Wooded],		1+CT_Wooded),
	MenuChoice(terrainStr[CT_Farmland],		1+CT_Farmland),
	MenuChoice(terrainStr[CT_Rough],			1+CT_Rough),
	MenuChoice(terrainStr[CT_Swamp],			1+CT_Swamp),
	MenuChoice(terrainStr[CT_Mountain],		1+CT_Mountain),
	MenuChoice(terrainStr[CT_Impassable],	1+CT_Impassable),
	MenuChoice(terrainStr[CT_Unused11],		1+CT_Unused11),
	MenuChoice(terrainStr[CT_Unused12],		1+CT_Unused12),
	MenuChoice(terrainStr[CT_Unused13],		1+CT_Unused13),
	MenuChoice(terrainStr[CT_Unused14],		1+CT_Unused14),
	MenuChoice(terrainStr[CT_Unused15],		1+CT_Unused15),
	MenuChoice("-", 								0 			),
	MenuChoice("Cancel", 						-1 		),
	MenuChoice(0,									0 			)
};

void CampaignEditControl::showColour()
{
	setupInfo();
	TextWindow& textWind = *control->textWin;
	textWind.setFont(Font_Info);
	textWind.setColours(Black);
	textWind.setHCentre(True);
	textWind.setPosition(Point(4,4));
	textWind.wprintf("Current:\r%s\r", terrainStr[terrain]);
}


void CampaignEditControl::terrainOverMap	(MapWindow* map, Event* event, MenuData* d, const Location* l)
{
	CTerrainVal newVal = world->terrain.get(*l);
	showColour();
	TextWindow& textWind = *control->textWin;
	textWind.setPosition(Point(4,60));
	textWind.wprintf("Over:\r%s\r", terrainStr[newVal]);

	if(event->buttons & Mouse::LeftButton)
	{
		world->terrain.set(*l, terrain);
		map->show1Terrain(*l, terrain);
		terrainChanged = True;
	}
	else if(event->buttons & Mouse::RightButton)
	{
		int result = menuSelect(0, terrainChoice, machine.mouse->getPosition());
		if(result > 0)
		{
			terrain = result - 1;
			showColour();
		}
	}
	else if(machine.mouse->buttons & Mouse::LeftButton)
	{
		world->terrain.set(*l, terrain);
		map->show1Terrain(*l, terrain);
		terrainChanged = True;
	}
}

void CampaignEditControl::terrainOffMap	()
{
	showColour();
}

void CampaignEditControl::terrainOptions	(const Point& p)
{
	dialAlert(0, p, "CampaignEditControl::terrainOptions not written", "OK");
}

void CampaignEditControl::saveTerrain()
{
	world->terrain.write();
}

/*
 * Terrain Image
 */

CampaignTerrain::CampaignTerrain()
{
}


void CampaignTerrain::init()
{
	readILBM(terrainFileName, &image, &pal);
#if 0
	int count = image.getWidth() * image.getHeight();
	UBYTE* ptr = image.getAddress();
	while(count--)
	{
		*ptr = *ptr + 1;
		ptr++;
	}
#endif
}

void CampaignTerrain::write()
{
	writeILBM(terrainFileName, image, pal);
	terrainChanged = False;
}

CampaignTerrain::~CampaignTerrain()
{
}

CTerrainVal CampaignTerrain::get(const Location& l)
{
	int line = l.y / (65536 * 5);
	int column = l.x / (65536 * 5);

	if((line >= 0) && (line < image.getHeight()) && (column >= 0) && (column < image.getWidth()))
		return *image.getAddress(column, line);
	else
		return CT_Impassable;

}

void CampaignTerrain::set(const Location& l, CTerrainVal type)
{
	int line = l.y / (65536 * 5);
	int column = l.x / (65536 * 5);

	if((line >= 0) && (line < image.getHeight()) && (column >= 0) && (column < image.getWidth()))
		*image.getAddress(column, line) = type;
}


