/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Display contents of database ptr.* file
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#define SHOW_POINTER_EXE
// #define CHECK_FILE_EXE

#include <stdio.h>
#include "types.h"
#include "string.h"

/*
 * Structure to store the pointers in
 */

struct KeywordData {
	UBYTE format;
	ULONG offset;
	UWORD picCount;
	UWORD sVar;				// Dont know what this is!
};

struct Keyword {
	Keyword* next;
	char* name;
	KeywordData data;
};

Keyword* keywords = 0;
Keyword* last = 0;



int loadKeywords(const char* fileName)
{
	int error = -1;

	FILE* fp = fopen(fileName, "rb");
	if(fp)
	{
 		char nameBuf[100];
 		int i = 0;
 
		while(!feof(fp))
		{

			char c = fgetc(fp);

			if(c == '~')
			{
				c = fgetc(fp);
				if(c == '~')
					c = '~';
				else
				{
						// We've got the name, now get the data
					nameBuf[i] = 0;
					i = 0;

					KeywordData data;
					data.format = c;
					fread(&data.offset, sizeof(data.offset), 1, fp);
					fread(&data.picCount, sizeof(data.picCount), 1, fp);
					fread(&data.sVar, sizeof(data.sVar), 1, fp);

					/*
					 * Loop around making any multiple keywords into seperate entries.
					 */

					Boolean more = True;
					char* name = nameBuf;
					while(more)
					{
						char* s = name;
						while(*s && (*s != '~'))
							s++;

						if(*s)
							*s++ = 0;
						else
							more = False;

						Keyword* k = new Keyword;

						k->name = strdup(name);
						k->data = data;
						k->next = 0;
						if(last)
							last->next = k;
						else
							keywords = k;
						last = k;
						name = s;
					}

					c = fgetc(fp);

					if(c != '@')
					{
						printf("Missing @ after %s\n\n", nameBuf);
						break;
					}

					continue;

				}
			}

			if(c != 0)
				nameBuf[i++] = c;
		}

		fclose(fp);

		error = 0;
	}
	else
		printf("Can't open %s\n", fileName);

	return error;
}

#ifdef SHOW_POINTER_EXE

/*
 * Usage Message display
 */

void usage()
{
	printf("Usage:\n  showptr filename\n\n");
}

/*
 * Main function
 */

void main(int argc, char* argv[])
{
	if(argc != 2)
	{
		usage();
		return;
	}

	if(loadKeywords(argv[1]) == 0)
	{
		/*
		 * Display them...
		 */

		Keyword* k = keywords;
		while(k)
		{
			printf("%c %8lx %2d %5d : '%s'\n",
				k->data.format,
				k->data.offset,
				(int) k->data.picCount,
				(int) k->data.sVar,
				k->name);

			k = k->next;
		}
	}
}

#endif

#ifdef CHECK_FILE_EXE

Boolean isKeyword(const char* word)
{
	Keyword* k = keywords;
	while(k)
	{
		if(!strcmp(word, k->name))
			return True;
		else if(!stricmp(word, k->name))
		{
			printf("%s matches but has wrong case\n", word);
			return True;
		}

		k = k->next;
	}

	return False;
}

/*
 * Check files to check that anything enclosed in @...@ is
 * one of the keywords
 */

void checkFile(const char* fileName)
{
	FILE* fp = fopen(fileName, "r");

	if(fp)
	{
		char buffer[100];
		int i = 0;
		char last = 0;

		enum Mode {
			Waiting,
			Filling
		};

		Mode mode = Waiting;

		while(!feof(fp))
		{
			char c = fgetc(fp);

			if(mode == Waiting)
			{
				if( (c == '@') && (last != '@') )
					mode = Filling;
			}
			else	// must be filling
			{
				if(c == '@')
				{
					if(i != 0)
					{
						buffer[i] = 0;

						if(!isKeyword(buffer))
							printf("'%s' is not a keyword\n", buffer);
#ifdef VERBOSE
					 	else
					 		printf("%s is OK\n", buffer);
#endif
					 }

					 mode = Waiting;
					 i = 0;
				}
				else
				{
					if(i >= 100)
					{
						printf("Keyword too long\n");
					}
					else
						buffer[i++] = c;
				}
			}

			last = c;
		}

		if(mode == Filling)
			printf("Mismatched @..@\n");

		fclose(fp);

	}
	else
		printf("Can't open %s\n", fileName);
}

/*
 * Usage Message display
 */

void usage()
{
	printf("Usage:\n  checkdb textFileName ptrFileName\n\n");
}

/*
 * Main function
 */

void main(int argc, char* argv[])
{
	if(argc != 3)
	{
		usage();
		return;
	}

	if(loadKeywords(argv[2]) == 0)
		checkFile(argv[1]);
}

#endif
