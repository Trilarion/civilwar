/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	3D Polygon shapes
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.8  1994/04/11  21:28:35  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/04/11  13:36:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1993/12/14  23:29:01  Steven_Green
 * Modifications to prepare for textured polygons
 *
 * Revision 1.5  1993/12/10  16:06:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1993/12/04  16:22:51  Steven_Green
 * Use of MemPtr to force deallocation of Point3D[]
 *
 * Revision 1.3  1993/12/01  15:11:02  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1993/11/26  22:30:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/11/25  04:40:06  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "poly3d.h"
#include "polygon.h"
#include "polyclip.h"
#include "shplist.h"
#include "view3d.h"
#include "memptr.h"

/*
 * Draw a 3D shape onto the mouse aware screen
 */

Point3D Poly3D::draw(ObjectDrawData* drawData, const Position3D& position)
{
	/*
	 * Convert 3D Points into 2D points
	 */

	Point3D* p3d = points;
	Point3DIndex pointCount = nPoints;

	MemPtr<Point3D> points3D(pointCount);

	if(!points3D)
		throw NoMemory();

	Point3D* p3dT = points3D;

	while(pointCount--)
	{
		/*
		 * Adjust for objects rotation
		 */

		*p3dT = *p3d++ * position;

		/*
		 * Adjust for objects location
		 */

		*p3dT += position;

		/*
		 * Adjust to Viewpoint origin and screen centre
		 */

		drawData->view->transform(*p3dT);

		p3dT++;
	}

	/*
	 * Convert faces into polygons
	 */


	Face* facePtr = faces;
	FaceIndex faceCount = nFaces;

	for(; faceCount--; facePtr++)
	{
		EdgeFace* edgePtr = facePtr->edges;

		/*
		 * Do back plane check
		 */

		Edge* edge = &edges[edgePtr[0].index];

		Point3D p1 = points3D[edge->first(edgePtr[0].direction)] -
					  points3D[edge->last(edgePtr[0].direction)];

		edge = &edges[edgePtr[1].index];
		Point3D p2 = points3D[edge->last(edgePtr[1].direction)] -
					  points3D[edge->first(edgePtr[1].direction)];

		long z = long(p1.x * p2.y) - long(p1.y * p2.x);

		if(z > 0)
		{
			EdgeIndex edgeCount = facePtr->nEdges;
			Polygon3D* poly = new Polygon3D(edgeCount * 2 + 6);
			poly->setColour(facePtr->colour);
			poly->setTexture(facePtr->pattern);
			Cord3D dist = -0x7fff;		// Find furthest polygon point

			{
				PolyClip3D clip(poly, drawData->view);	// Clipping Information

				for(; edgeCount--; edgePtr++)
				{
					Edge* edge = &edges[edgePtr->index];

					Point3DIndex point = edge->first(edgePtr->direction);
					Point3D& pt = points3D[point];

					clip.clipPoint(pt);

					if(pt.z > dist)
						dist = pt.z;

				}

				// clip is closed while being destructed
			}

			if(poly->countPoints() >= 3)
				drawData->add(poly, dist);
			else
				delete poly;
		}
	}

	return Point3D(0,0,0);
}



