/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Tables used by all parts of game
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "tables.h"

Speed regimentMarchSpeeds[3][4] = {
	{ YardsPerHour(5280), YardsPerHour(4500), YardsPerHour(5500), YardsPerHour(4500) },	// Infantry
	{ YardsPerHour(7000), YardsPerHour(8000), YardsPerHour(7500), YardsPerHour(6500) },	// Cavalry
	{ YardsPerHour(6000), YardsPerHour(6500), YardsPerHour(7000), YardsPerHour(6000) },	// Artillery
};

Speed commanderMarchSpeed = MilesPerHour(5);	// Fast!	(used to be 12)


/*
 * Names of Regiment types
 */

LGE infantryTypeNames[] = {
	lge_RegularInfantry,
	lge_MilitiaInfantry,
	lge_Sharpshooter,
	lge_Engineer,
	lge_RailEngineer
};

LGE cavalryTypeNames[] = {

	lge_RegularCavalry,
	lge_MilitiaCavalry,
	lge_RegularMountedInfantry,
	lge_MilitiaMountedInfantry,
	lge_Raiders,

};

LGE artilleryTypeNames[] = {
	
	lge_SmoothboreArtillery,
	lge_LightArtillery,
	lge_RifledArtillery,
	lge_SiegeArtillery,

};

/*
 * Attribute Enumerations
 */

// const char* rankNames[] = {
const LGE rankNames[] = {
	lge_Army,			// lge_President,
	lge_Army,
	lge_Corps,
	lge_Division,
	lge_Brigade,
	lge_Regiment
};

ValueEnum fatigueLevels[] = {
	{ lge_Fresh, 		 75  },
	{ lge_Weary, 		125  },
	{ lge_Tired, 		200  },
	{ lge_Exhausted, 	  0  }
};

ValueEnum experienceLevels[] = {
	{ lge_Untried,		  1 },
	{ lge_Tried,		100 },
	{ lge_Seasoned,	140 },
	{ lge_Veteran,		200 },
	{ lge_BattleWeary,  0 }
};


ValueEnum moraleLevels[] = {
	{ lge_Routing,		  1 },
	{ lge_Demoralised, 50 },
	{ lge_Dispirited,	100 },
	{ lge_Normal,		140 },
	{ lge_Good,			200 },
	{ lge_High,			  0 }
};


ValueEnum efficiencyLevels[] = {
	{ lge_Unefficient,		50 },
	{ lge_Bumbling,		  100 },
	{ lge_Adequate,		  140 },
	{ lge_Efficient,		  200 },
	{ lge_VeryEfficient,	 0 }
};

ValueEnum abilityLevels[] = {
	{ lge_Useless, 		 50 },
	{ lge_ability1,		100 },
	{ lge_ability2,	  	140 },
	{ lge_ability3,	  	200 },
	{ lge_Veryable,		  0 }
};

ValueEnum aggressionLevels[] = {
	{ lge_Peaceful,					50 },
	{ lge_Slightlyaggressive,  100 },
	{ lge_aggressive,	  		  140 },
	{ lge_Veryaggressive,	  	  200 },
	{ lge_Extremelyaggressive,	 0 }
};

ValueEnum supplyLevels[] = {
	{ lge_Unsupplied,			     1 },
	{ lge_PoorlySupplied,		 75 },
	{ lge_PartiallySupplied,	200 },
	{ lge_Supplied,				  0 }
};

/*=======================================================
 * Attribute Enumeration Support functions
 */

const char* getAttributeName(ValueEnum* table, AttributeLevel level)
{
	while(table->range && (level > table->range))
		table++;
	return language(table->name );
}

UBYTE getAttributeEnum(ValueEnum* table, AttributeLevel level)
{
	int i = 0;
	while(table[i].range && (level > table[i].range))
		i++;
	return i;
}

/*==================================================================
 * Get the current order as a string
 *
 * NB: This table must match the OrderMode enumeration in orders.h!
 */

static const LGE orderNames[] = {

	lge_Advance,
	lge_CautiousAdvance,
	lge_Attack,
	lge_Stand,
	lge_Hold,
	lge_Defend,
	lge_Withdraw,
	lge_Retreat,
	lge_HastyRetreat,

	lge_IllegalOrder

};

static const LGE moveNames[] = {

	lge_MovebyLand,
	lge_MovebyRail,
	lge_MovebyRiver,
	lge_MovebySea,

};

const char* getOrderStr(const Order& order)
{
	if(order.how != ORDER_Land)
		return language( moveNames[order.how] );

	OrderMode orderMode = order.mode;
	if(orderMode >= ORDER_Max)
		orderMode = ORDER_Max;

	return language( orderNames[orderMode] );
}

const char* getOrderStr(OrderMode mode)
{
	return language( orderNames[mode] );
}

const char* getOrderStr(const BattleOrder& order)
{
	OrderMode orderMode = order.mode;
	if(orderMode >= ORDER_Max)
		orderMode = ORDER_Max;

	return language( orderNames[orderMode] );
}

/*
 * Brigade Combat Values
 *
 * It is really the %ge chance of each man causing a casualty in a battle.
 */

int BrigadeCombatValue[3][4] = {
#if 0
	{ 2, 1, 3, 2 },				// Infantry
	{ 1, 1, 3, 2 },				// Cavalry
	{ 20, 10, 30, 50 }			// Artillery
#else
	{  10,  5,  15,  10 },				// Infantry
	{   5,  5,  15,  10 },				// Cavalry
	{ 100, 50, 150, 250 }			// Artillery
#endif
};


#ifdef DEBUG

const char* getBoolStr(Boolean flag)
{
	return flag ? "True" : "False";
}


#endif

const char* sideStr[] = {
		"None",
		"USA",
		"CSA",
		"Both"
};

const char* getSideStr(Side side)
{
	return sideStr[side];
}
