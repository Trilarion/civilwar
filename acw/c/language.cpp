/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	This routine loads the text asscoiated with a particular language
 * from a special lge data file.
 * 
 * The lge file is a file where the first two bytes is the number of 
 * text entries, followed by a list of offset pointers, and lastly 
 * the actual text.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 * 17 Jan 95: Fiddled about with so that:
 *              - CD Path is loaded at start instead of having to be
 *                reloaded from the install program for every file.
 *              - Install information moved into a class so that it
 *                can be correctly initialised and destructed automatically
 *              - setup_language automatically invoked if any of the
 *                global functions are used.
 *
 *----------------------------------------------------------------------
 */


#include <string.h>
#include <fstream.h>
#include <io.h>
#include <conio.h>
#include <stdlib.h>
#include "cd_open.h"
#include "strutil.h"
#include "memptr.h"
#include "language.h"
#include "dialogue.h"

struct Install {
	char lang[4];		// Extension used for language files
	char* cdPath;		// Where the CD is (including backslash)
	FILE* langFile;	// Language text file kept open to speed things up.
	UWORD nStrings;	// Numebr of strings in language file
	ULONG* stringPos;	// Offsets to strings in file

	Boolean initialised;

	Install();

	~Install();

	void setup_language();

	void setup()
	{
		if(!initialised)
			setup_language();
	}
};




/*
 * Stored as global, so auto destructed on exit
 */

Install install;		// Installation Information
static char fileStr[FILENAME_MAX];
static char noString[] = "";		// Empty String to use in error condition


static const int MaxStringsInMemory = 32;
static const char MaxStrLength = 120;

class StringManager {
	char* strings[MaxStringsInMemory];
	LGE id[MaxStringsInMemory];
	int next;
public:
	StringManager();
	~StringManager();

	char* find(LGE n);
	char* make(const char* s, LGE n);
	char* makeSpace(size_t length, LGE n);
};

StringManager::StringManager()
{
	for(int i = 0; i < MaxStringsInMemory; i++)
	{
		strings[i] = 0;
		id[i] = lge_NULL;
	}
}

StringManager::~StringManager()
{
	for(int i = 0; i < MaxStringsInMemory; i++)
	{
		delete[] strings[i];
		id[i] = lge_NULL;
		strings[i] = 0;
	}
}

char* StringManager::make(const char* s, LGE n)
{
	if(strings[next])
		delete[] strings[next];
	char* newString = strings[next] = copyString(s);
	id[next] = n;
	if(++next >= MaxStringsInMemory)
		next = 0;
	return newString;
}

char* StringManager::makeSpace(size_t length, LGE n)
{
	if(strings[next])
		delete[] strings[next];
	char* newString = strings[next] = new char[length + 1];
	id[next] = n;
	if(++next >= MaxStringsInMemory)
		next = 0;
	return newString;
}

char* StringManager::find(LGE n)
{
	for(int i = 0; i < MaxStringsInMemory; i++)
	{
		if(strings[i] && (id[i] == n))
		{
			char* newString = strings[i];

			/*
			 * Swap around, so that string is in next position
			 */

			if(i != next)
			{
				strings[i] = strings[next];
				strings[next] = newString;
				id[i] = id[next];
				id[next] = n;
			}
			if(++next >= MaxStringsInMemory)
				next = 0;

			return newString;
		}
	}
	return 0;
}

static StringManager languageStrings;

char* language(LGE msg)
{
	char* s = languageStrings.find(msg);
	if(s)
		return s;

	install.setup();

	/*
	 * New Version By Steven
	 */

	Boolean hadError = True;

	/*
	 * Note:
	 *   Would be quicker to keep file open all time
	 *   Would be even quicker to load in all the strings
	 *   at the start of the game!!!
	 */

	if(install.langFile)
	{
		ULONG pos = install.stringPos[msg];

		if(fseek(install.langFile, pos, SEEK_SET))
			goto getOut;

		UWORD length;

		if(fread(&length, sizeof(length), 1, install.langFile) != 1)
			goto getOut;

		s = languageStrings.makeSpace(length, msg);

		if(fread(s, length, 1, install.langFile) != 1)
			goto getOut;

		s[length] = 0;

		hadError = False;
	getOut:
		// fclose(fp);
		;
	}

	if(hadError || !s)
		return noString;
	else
		return s;

}

Boolean isLanguage(const char* ext)
{
	install.setup();
	return stricmp(ext, install.lang) == 0;
}


Install::Install()
{
 	initialised = False;
 	strcpy(lang, "ENG");
 	cdPath = 0;
 	langFile = 0;
}

Install::~Install()
{
	if(langFile)
		fclose(langFile);
	if(cdPath)
		delete[] cdPath;
	delete[] stringPos;
}

void Install::setup_language()
{
	if(!install.initialised)
	{
		install.initialised = True;		// Flag to 

		ifstream installdat;

		installdat.open( "install.dat", ios::in | ios::binary );

		if(installdat ) 
		{
			char temp = 0;

			int count = 0;

			int pathCount = 0;

			installdat.seekg( 0, ios::end);
			int tsize = installdat.tellg ();

			installdat.seekg( 5, ios::beg );

			// getAfterKeyword( installdat, "PATH=" );

			MemPtr<char> buffer(100);

			while( temp != '\r' && count < tsize )
			{
				installdat.read ( ( unsigned char *)&temp, sizeof ( char ) );

				if ( temp != '\r' ) buffer[pathCount++] = temp;
				count++;
			}

			if(pathCount)
			{
#if !defined(COMPUSA_DEMO)
				if ( buffer[pathCount-1] != '\\' ) buffer[pathCount++] = '\\';
				buffer[pathCount] = '\0';
				install.cdPath = copyString(buffer);
#endif
			}

			installdat.seekg( 5, ios::beg );    // restore read ptr position;

			count = 5;

			/*
	 	 	 * 1st line is the path of the CD...
	 	 	 */


			temp = 'a';

			while( temp != '\r' && count < tsize )
			{
				installdat.read ( ( unsigned char *)&temp, sizeof ( char ) );

				count++;
			}


			while( temp != 'L' && count < tsize )
			{
				installdat.read ( ( unsigned char *)&temp, sizeof ( char ) );

				count++;
			}

			installdat.seekg( 8, ios::cur );


			/*
		 	 * 2nd line is language extension
		 	 */

			temp = 0;

			if ( count != tsize ) count = 0;

			while( temp != '\r' && count < 3 )
			{
				installdat.read ( ( unsigned char *)&temp, sizeof ( char ) );

				if ( temp >= 'A' && temp <= 'Z' ) 
				{
					install.lang[ count++ ] = temp;
				}
			}

			install.lang[ count ] = '\0';

			installdat.close();
		}

		/*
		 * Open language file
		 */

		const char* lgeName = getFileName("dbdata\\language");

		install.langFile = FileOpen(lgeName, "rb");

		fread(&install.nStrings, sizeof(install.nStrings), 1, install.langFile);
		install.stringPos = new ULONG[install.nStrings];
		fread(install.stringPos, sizeof(ULONG), install.nStrings, install.langFile);
	}
}


const char* getFileName( char* name )
{
	install.setup();

	static char langStr[FILENAME_MAX];

	strcpy( langStr, name );
	strcat( langStr, "." );
	strcat( langStr, install.lang );

	return langStr;
}	


Boolean fileOpen(ifstream& txt, const char *name, const int mode)
{
	install.setup();


	const char* s = getPathAndCheck(name);
	if(s)
	{
		txt.open(s, mode);
		if(txt)
			return True;
	}

	return False;
}

FILE* FileOpen( const char *name, const char* mode )
{
	install.setup();


	const char* s = getPathAndCheck(name);
	if(s)
		return fopen(s, mode);
	else
		return 0;
}

int fileOpen( const char *name, int mode )
{
	install.setup();


	const char* s = getPathAndCheck(name);
	if(s)
		return open(s, mode);
	else
		return -1;

}


const char* getPathAndCheck(const char* name)
{
	for(;;)
	{
		if(access(name, R_OK) == 0)
			return name;

#if !defined(COMPUSA_DEMO)
		install.setup();

		if(install.cdPath)
		{
			strcpy(fileStr, install.cdPath);
			strcat(fileStr, name);

			if(access(fileStr, R_OK) == 0)
				return fileStr;
		}
#endif

		MemPtr<char> buffer(512);
		sprintf(buffer, language(lge_FileMissing), name);
		if(dialAlert(0, buffer, language(lge_RetryCancel)) != 0)
			throw GeneralError(language(lge_FileMissing), name);
	}
}


const char* addPath( const char* path )
{ 
	install.setup();

	MemPtr<char> buffer(MaxStrLength);
#if !defined(COMPUSA_DEMO)
	if(install.cdPath)
		strcpy(buffer, install.cdPath);
	else 
#endif
		buffer[0] = '\0';
 
	strcat(buffer, path);

	return languageStrings.make(buffer, lge_NULL);
}

Boolean DoesFileExist( const char* fileName )
{
	return (access(fileName, R_OK) == 0);
}



