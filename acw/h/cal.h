/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef CAL_H
#define CAL_H

#ifndef __cplusplus
#error cal.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	CAL (Current Army List) Screen
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.15  1994/04/22  22:16:24  Steven_Green
 * Icon Handling Tidied Up
 *
 * Revision 1.1  1994/02/09  15:01:35  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

// #include "procfunc.h"

class OrderBattle;
class Unit;
class MenuData;

Unit* doCampaignCAL(OrderBattle* ob, Unit* startUnit, int* giveOrder, MenuData* proc);
Unit* doBattleCAL(OrderBattle* ob, Unit* startUnit, int* giveOrder, MenuData* proc);

#endif /* CAL_H */

