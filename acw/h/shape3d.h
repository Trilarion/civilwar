/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef SHAPE3D_H
#define SHAPE3D_H

#ifndef __cplusplus
#error shape3d.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * 3D Shape Class
 *
 * Note: Uses LEFT hand coordinate system:
 *                
 *						Y
 *
 *                ^	 Z
 *						| _
 *						| /
 *						|/
 * 					+----> X
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.25  1994/08/09  15:46:26  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.24  1994/06/24  14:45:10  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.23  1994/06/21  18:45:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.22  1994/06/09  23:36:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.21  1994/06/07  18:33:23  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.20  1994/06/06  13:20:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.19  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.18  1994/05/21  13:18:12  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.17  1994/05/19  17:47:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.16  1994/04/20  22:24:21  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.15  1994/04/11  21:31:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.14  1994/04/11  13:38:20  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.13  1994/04/05  12:29:47  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.12  1994/03/21  21:04:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.11  1994/03/18  15:08:40  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.10  1994/02/28  23:05:43  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1994/02/17  20:00:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1993/12/23  09:29:10  Steven_Green
 * Mods for 3D Sprites
 *
 * Revision 1.7  1993/12/21  00:34:28  Steven_Green
 * Changes to prepare for 3D Sprites
 *
 * Revision 1.6  1993/12/16  22:20:15  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1993/12/10  16:08:02  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1993/11/25  04:40:34  Steven_Green
 * Seperated up to allow different types of 3D object
 *
 * Revision 1.3  1993/11/24  09:34:12  Steven_Green
 * Cord3D made into longs.
 *
 * Revision 1.2  1993/11/19  19:01:25  Steven_Green
 * Point3D operators defined better using & and const.
 *
 * Revision 1.1  1993/11/16  22:43:10  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "types3d.h"

#include "ptrlist.h"
#include "gr_types.h"
#include "miscfunc.h"
#include "error.h"
#include "palette.h"
#include "measure.h"


class Terrain;
class ViewPoint;
class LightSource;
class Region;
class Shape3D;
class Wangle3D;
typedef UWORD Point3DIndex;
typedef UWORD EdgeIndex;
typedef UWORD FaceIndex;

// typedef BattleCord Cord3D;


class ObjectDrawData;

class Shape3D {
public:
	virtual Point3D draw(ObjectDrawData* drawData, const Position3D& position);
	virtual ~Shape3D() {	}
};


class WorldObject : public Position3D {
	Shape3D* shape;
public:
	WorldObject(Shape3D* o, Position3D p) : Position3D(p) { shape = o; };
	WorldObject(Shape3D* o) : Position3D() { shape = o; };

	void setRotate(Wangle3D& r) { Wangle3D::operator =(r); };

	friend void drawObject3D(WorldObject* ob, void* data);
};

// typedef PtrSList<WorldObject> ObjectList;
// typedef PtrSList<WorldObject> WorldObjectList;

class WorldObjectList : public PtrSList<WorldObject> {
public:
	~WorldObjectList();

	void draw(Region* bm, ViewPoint* view, LightSource* l, Terrain* terrain);
};


// void drawObjectList(ViewPoint* view, WorldObjectList* ob, LightSource* l = 0);


#endif /* SHAPE3D_H */

/* Depends on h\measure.h , touched on 04-07-94 at 10:13:49 */
/* Depends on h\measure.h , touched on 04-15-94 at 11:56:01 */
/* Depends on h\measure.h , touched on 04-15-94 at 14:52:27 */
/* Depends on h\trig.h , touched on 05-17-94 at 14:52:10 */
/* Depends on h\trig.h , touched on 05-17-94 at 15:09:26 */
/* Depends on h\measure.h , touched on 05-19-94 at 16:22:21 */
/* Touched on 05-19-94 at 17:37:08 */
/* Touched on 05-31-94 at 08:52:56 */
/* Touched on 05-31-94 at 17:15:17 */
/* Touched on 06-01-94 at 10:53:33 */
/* Touched on 06-01-94 at 12:50:50 */
/* Touched on 06-03-94 at 17:19:42 */
/* Touched on 06-03-94 at 17:58:54 */
/* Touched on 06-07-94 at 11:44:46 */
/* Touched on 06-07-94 at 11:50:04 */
/* Touched on 06-08-94 at 12:26:33 */
/* Touched on 06-09-94 at 12:19:07 */
/* Touched on 06-09-94 at 18:26:42 */
/* Touched on 06-10-94 at 14:30:25 */
/* Touched on 06-13-94 at 13:06:30 */
/* Touched on 06-13-94 at 16:05:12 */
/* Touched on 06-23-94 at 15:55:34 */
/* Touched on 06-23-94 at 16:29:20 */
