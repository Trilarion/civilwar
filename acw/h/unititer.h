/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef UNITITER_H
#define UNITITER_H

#ifndef __cplusplus
#error unititer.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Method of iterating through Order of Battle
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "gamedef.h"

class Unit;

class UnitIter {
	Unit* top;					// Top Unit in tree
	Unit* current;				// Current Unit
	Rank hiRank;
	Rank lowRank;				// Lowest Rank to use
	Boolean independant;		// Set if only independant units to be returned
	Boolean realDetached;	// Only real detached units

public:
	void setup(Unit* u, Boolean onlyIndependant, Boolean real, Rank highest, Rank lowest);

	UnitIter(Unit* u, Boolean onlyIndependant, Boolean real, Rank highest, Rank lowest)
	{
		setup(u, onlyIndependant, real, highest, lowest);
	}

	void reset();

	/*
	 * Return True if can get a unit
	 */

	Boolean ok()
	{
		return (current != 0);
	}

	Unit* get();			// Get unit and advance to next
private:
	void getNext();
};


#endif /* UNITITER_H */

