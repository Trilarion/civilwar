/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef COMBVAL_H
#define COMBVAL_H

#ifndef __cplusplus
#error combval.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Calculate Combat Value
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "gamedef.h"

class Unit;

#if !defined(TESTBATTLE)
class Facility;
#endif

CombatValue calc_UnitCV(Unit* uit, Boolean real);

#if !defined(TESTBATTLE)
CombatValue calc_townCV(Facility* f, Boolean includeOccupier);
CombatValue calc_townNavalCV(Facility* f);
#endif

#endif /* COMBVAL_H */

