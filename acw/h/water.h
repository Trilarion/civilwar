/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef WATER_H
#define WATER_H

#ifndef __cplusplus
#error water.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	River and Sea Zones in Campaign Game
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.8  1994/08/09  15:46:26  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/07/28  19:01:36  Steven_Green
 * Naval Movement Implemented
 *
 * Revision 1.6  1994/07/25  20:34:58  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/07/04  13:31:45  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/06/24  14:45:10  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/06/21  18:45:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/06/09  23:36:46  Steven_Green
 * Added writeData functions.
 *
 * Revision 1.1  1994/06/07  18:33:23  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "naval.h"
#include "mapob.h"
#include "gamedef.h"
#include "array.h"

#if defined(DEBUG)
class CampaignWorld;
#endif

class MapWindow;
class WaterNetwork;
class Unit;
class Facility;
class ChunkFileWrite;
class OrderBattle;

enum WaterZoneType {
	WZ_Coast, WZ_Sea, WZ_River				// WZ_None
};

/*
 * Base class for Any water Zone
 *
 * Boats are owned by whichever side owns the zone
 */

class WaterZone : public MapObject {
public:
	Flotilla boats;				// What is here and staying here?
	FleetList fleets;				// Boats moving out of here
	Side side;						// Who owns this zone?

	WaterZoneType type;			// Coast, Sea or River?
#ifdef CAMPEDIT
	DynamicArray<WaterZoneID> connections;
#else
	UBYTE connectCount;
	UWORD connections;			// Entry in connections list
#endif

#ifdef CAMPEDIT
	DynamicArray<FacilityID> facilityList;
#else
	UBYTE facilityCount;				// Number of facilities in this zone
	FacilityListID facilityList;	// Entry in the facility list table
#endif


#ifdef CHRIS_AI
#if defined(NOBITFIELD)
	Boolean AI_NeedReinforcing;
#else
	Boolean AI_NeedReinforcing:1;
#endif
	// Boolean AI_worrying:1;
#endif

#ifdef DEBUG
	CombatValue ai_cvNeeded;
	UBYTE ai_priority;
#endif

public:
	WaterZone();
	~WaterZone();

	/*
	 * Overload the mapobject virtual functions
	 */

	void mapDraw(MapWindow* map, Region* bm, Point where);
	MapPriority isVisible(MapWindow* map) const;
	void showCampaignInfo(Region* window);
	void placeInfo(Region* window, Flotilla* fleet);
	void placeInfo(Region* window, NavalType type, Flotilla* fleet);

#ifdef CAMPEDIT
	void showInfo(Region* window);
#else
	Boolean isEnemy(Side s);
	Boolean fightNavalBattle(Flotilla& boats);

	WaterZoneID getID(const WaterNetwork* net) const;
	WaterZoneID getRetreatZone(Side side) const;
	FacilityID getEmergencyLand(Side side) const;
#endif
};


/*
 * Structure used by route algorithm
 */

struct WaterRouteNode {
	WaterZoneID from;		// Where did we come from to get here?
	GameTicks cost;		// How far is it to here?
};

struct WaterRoute {
	WaterRouteNode* route;

	WaterRoute() { route = 0; }
	~WaterRoute() { if(route) delete[] route; }

	void drawRoute(MapWindow* map, Region* bm, WaterZoneID from, WaterZoneID to) const;
};


/*
 * Ferries (Troops in transit by water)
 */

class Ferry {
	friend class WaterNetwork;
	friend void writeDynamicWater(ChunkFileWrite& file, WaterNetwork& water, const OrderBattle& ob);
	friend void updateWater(ChunkFileRead& file, WaterNetwork& water, const OrderBattle& ob);

	Ferry* next;

	WaterZoneID lastZone;	// Where it is (or was)
	WaterZoneID nextZone;	// Where it is moving to
	FacilityID destination;	// Where the passengers should be dropped off
	TimeBase arrival;		// When they will arrive at the next location
	Side side;					// What side it is on
	Unit* passenger;			// Who is being carried?

	enum FerryMode { FERRY_Moving, FERRY_Landing } mode;

public:
	Ferry();
	~Ferry();

#ifndef CAMPEDIT
	void land(Facility* facility, WaterZone* zone, const TimeBase& gameTime);
#endif
};


/*
 * Overall structure for water network
 */

struct WaterRouteVars;

class WaterNetwork {
	friend class DataFileOutStream;
	friend class DataFileInStream;
	friend class CampaignWorld;
	friend void writeStaticWater(ChunkFileWrite& file, WaterNetwork& water);
	friend void writeDynamicWater(ChunkFileWrite& file, WaterNetwork& water, const OrderBattle& ob);
	friend void readWater(ChunkFileRead& file, WaterNetwork& water);

	friend class WaterZone;

	TimeBase lastTime;			// Last time railways were processed

	Ferry* ferries;					// Units being ferried

#ifndef CAMPEDIT
	void searchForBombardable();
	void BombardPort( Facility* f, WaterZone& zone);

#endif

#ifdef CAMPEDIT
public:				  
	DynamicArray<WaterZone> zones;

#else
private:
	Array<FacilityID> facilityList;	// List of facility IDs used by river/coast
	Array<WaterZoneID> connectList;	// List of connections
public:				  
	Array<WaterZone> zones;

#endif

public:
	WaterNetwork();
	~WaterNetwork();

	void clear();
#ifdef DEBUG
	void showWaterZones(MapWindow* map, CampaignWorld* world);
#else
	void showWaterZones(MapWindow* map);
#endif

#ifndef CAMPEDIT
#ifdef DEBUG
	void updateWater(CampaignWorld* world);
#endif
#endif

#if !defined(CAMPEDIT)
	WaterZoneID getConnect(WaterZoneListID i)
	{
		return connectList[i];
	}
#endif

	WaterZoneID getID(const WaterZone* zone) const
	{
		return zones.getID(zone);
	}


#if !defined(CAMPEDIT)
	void process(const TimeBase& t);
#endif

	// Two functions assocciated with retreating

	void FindRetreatZone( WaterZone* wz, Side s );
	Boolean WaterEnemyAdjacent( WaterZone& zone );

#if !defined(CAMPEDIT)
	WaterZoneID findRoute(WaterZoneID from, WaterZoneID to, Boolean allowEnemy, const Flotilla* fleet, WaterRoute* routes);
	GameTicks findRouteTime(WaterZoneID from, WaterZoneID to, const Flotilla* fleet);

public:


	void sendFleet(Fleet* fleet, WaterZoneID from, WaterZoneID to, const TimeBase& gameTime);

	Facility* getFacility(FacilityListID id);

	void makeFerry(Unit* unit, FacilityID from, FacilityID to);
	void moveFerry(Ferry* ferry, const TimeBase& gameTime);
	void addFerry(Ferry* ferry);
	void removeFerry(Ferry* ferry);
	void removeUnit(Unit* u);

private:
	void processRoute(WaterZoneID id, WaterRouteVars& vars);
	void processFleets(WaterZone& zone, const TimeBase& gameTime);
#endif	// CAMPEDIT
};


GameTicks waterDistance(WaterZone* from, WaterZone* to, UBYTE baseSpeed);

#ifdef DEBUG
char* flotillaName(Flotilla* boats);
char* fleetName(Fleet* fleet);
#endif


#endif /* WATER_H */

