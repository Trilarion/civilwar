/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef ED_TROOP_H
#define ED_TROOP_H

#ifndef __cplusplus
#error ed_troop.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Troop Editor Interface
 *
 *----------------------------------------------------------------------
 */

class General;
class Unit;

void editGeneral(General* oldGeneral);
void editUnit(Unit* oldUnit);


#endif /* ED_TROOP_H */

