/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef TESTTIME_H
#define TESTTIME_H

#ifndef __cplusplus
#error testtime.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Test Timer
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.6  1994/03/11  23:14:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/03/10  14:29:08  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/03/01  22:30:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1993/12/10  16:08:02  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1993/12/01  15:12:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/11/30  02:57:48  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <strstrea.h>
#ifndef TIMER_H
#include "timer.h"
#endif


const int MaxTimer = 20;

class TestTimer {
	unsigned long lastCount;
	unsigned long start;
	unsigned long timers[MaxTimer];
	const char* description[MaxTimer];
	int nextTimer;
public:
	TestTimer() { };

	void init() { lastCount = start = timer->getFastCount(); nextTimer = 0; };
	void mark(const char *s);

	void show(ostrstream& buffer) const;
};

extern TestTimer testTimer;



#endif /* TESTTIME_H */

/* Depends on h\timer.h , touched on 03-01-94 at 09:27:54 */
/* Depends on h\timer.h , touched on 03-02-94 at 12:02:22 */
/* Depends on h\timer.h , touched on 03-10-94 at 17:03:41 */
/* Depends on h\timer.h , touched on 03-10-94 at 17:16:12 */
/* Depends on h\timer.h , touched on 03-10-94 at 17:27:30 */
