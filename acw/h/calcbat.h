/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef CALCBAT_H
#define CALCBAT_H

#ifndef __cplusplus
#error calcbat.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id  C.J.Wall 21/12/94 $
 *----------------------------------------------------------------------
 *
 *	This function calculates a battle Result.
 * If called from inside a battle pass it True.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"
#include "gamedef.h" 
#include "side.h"

class MarkedBattle;
class CampaignAI;
class Unit;
class Facility;

/*
 * Structure of information from before the battle
 */

struct PreBattleInfo {
	Strength CSAstrength;
	Strength USAstrength;
	Strength CSAstragglers;
	Strength USAstragglers;
	Strength CSAcasualties;
	Strength USAcasualties;

	void make(MarkedBattle* batl);
};

/*
 * Battle Result Structure
 */

enum BattleWinEnum {
	BW_RanAway,		// Loser Ran Away
	BW_Win,			// Normal Win
};

struct BattleResult {
	Side winner;				// USA, CSA or None!
	Side initiative;			// Who had initiative?
	BattleWinEnum how;		// What sort of result was it?

	Strength CSAstragglers;
	Strength USAstragglers;
	Strength CSAcasualties;
	Strength USAcasualties;

	Unit* USAsenior;
	Unit* CSAsenior;

	void make(MarkedBattle* bat, PreBattleInfo* preInfo, Boolean calculateBattle);
	void getTitle(char* buffer);
	void getBody(char* buffer);
	void getStats(char* buffer);
	void getButton(char* buffer);

private:
	Unit* getSenior(MarkedBattle* batl, Side si);

};

/*
 * Fortification Results
 */

Boolean calcFortBattle( MarkedBattle* batl, Facility* f );

/*
 *===================================================================*
 * Anything left below this is not used any more....					  	*
 *===================================================================*
 */

#ifdef CHRIS_OLD_STUFF
/*
 *  The function for Steve to call that'll calculate a battle result.
 *  The battleStarted is set if a battle in play is to be calculated.
 */

void calcBattle( Boolean battleStarted, MarkedBattle* batl, int CSATroops = 0, int USATroops = 0 );

int CalcNumCas( Side si, MarkedBattle* batl, Facility* f = NULL );

#endif	// CHRIS_OLD

#endif /* CALCBAT_H */

